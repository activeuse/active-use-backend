﻿namespace Service.Core.Exceptions
{
    using System;

    public class ItemNotFoundException : Exception
    {
        public ItemNotFoundException()
        {
            
        }

        public ItemNotFoundException(Exception ex, string message)
            : base(message, ex)
        {
        }

        public ItemNotFoundException(string message)
            : base(message)
        {
        }
    }
}