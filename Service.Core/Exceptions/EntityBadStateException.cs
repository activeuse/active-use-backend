﻿namespace Service.Core.Exceptions
{
    using System;

    public class EntityBadStateException : Exception
    {
        public EntityBadStateException()
        {
        }

        public EntityBadStateException(Exception ex, string message)
            : base(message, ex)
        {
        }

        public EntityBadStateException(string message)
            : base(message)
        {
        }
    }
}