﻿namespace Service.Core.Sql
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Threading.Tasks;

    public static class SqlExtensions
    {
        /// <summary>
        /// Reads the item.
        /// </summary>
        /// <typeparam name="T">Type of item to read</typeparam>
        /// <param name="command">The SQL command.</param>
        /// <param name="readFunc">The read function.</param>
        /// <returns>Read item.</returns>
        public static async Task<T> ReadItem<T>(this DbCommand command, Func<DbDataReader, T> readFunc)
        {
            using (var reader = await command.ExecuteReaderAsync())
            {
                if (await reader.ReadAsync())
                {
                    return readFunc(reader);
                }

                return default(T);
            }
        }

        /// <summary>
        /// Reads items using provided reader function.
        /// </summary>
        /// <typeparam name="T">Type of item to read</typeparam>
        /// <param name="command">The SQL command.</param>
        /// <param name="readFunc">The reader function.</param>
        /// <returns>Read result.</returns>
        public static async Task<IEnumerable<T>> ReadItems<T>(this DbCommand command, Func<DbDataReader, T> readFunc)
        {
            var resultList = new List<T>();
            using (var reader = await command.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    resultList.Add(readFunc(reader));
                }

                return resultList;
            }
        }

        /// <summary>
        /// Reads the item.
        /// </summary>
        /// <typeparam name="T">Type of item to read</typeparam>
        /// <param name="command">The SQL command.</param>
        /// <param name="readFunc">The read function.</param>
        /// <returns>Read item.</returns>
        public static T ReadItemSync<T>(this DbCommand command, Func<DbDataReader, T> readFunc)
        {
            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    return readFunc(reader);
                }

                return default(T);
            }
        }

        /// <summary>
        /// Reads the nullable value.
        /// </summary>
        /// <typeparam name="T">Type of result value</typeparam>
        /// <param name="reader">The SQL data reader.</param>
        /// <param name="key">The key.</param>
        /// <returns>Item or null</returns>
        public static T ReadNullable<T>(this IDataReader reader, string key)
        {
            var t = typeof(T);
            if (t.IsClass)
            {
                return reader[key] == DBNull.Value ? default(T) : (T)Convert.ChangeType(reader[key], t);
            }

            var underlyingType = Nullable.GetUnderlyingType(t);

            if (underlyingType != null)
            {
                if (underlyingType.IsEnum)
                {
                    return reader[key] == DBNull.Value ? default(T) : (T)Enum.ToObject(underlyingType, reader[key]);
                }

                return reader[key] == DBNull.Value ? default(T) : (T)Convert.ChangeType(reader[key], underlyingType);
            }

            return (T)Convert.ChangeType(reader[key], t);
        }
    }
}