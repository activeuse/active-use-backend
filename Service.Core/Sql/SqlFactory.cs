﻿namespace Service.Core.Sql
{
    using System;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    /// <summary>
    /// SQL connection manager
    /// </summary>
    public static class SqlFactory
    {
        /// <summary>
        ///     Creates the connection.
        /// </summary>
        /// <returns>
        ///     <see cref="SqlConnection" />
        /// </returns>
        public static async Task<SqlConnection> CreateConnection(string connectionStringName)
        {
            var sqlConnection = new SqlConnection(connectionStringName);
            try
            {
                await sqlConnection.OpenAsync();
            }
            catch (Exception exception)
            {
                throw new Exception(
                    "An error occurred while connecting to the database. See innerException for details.",
                    exception);
            }

            return sqlConnection;
        }
    }
}