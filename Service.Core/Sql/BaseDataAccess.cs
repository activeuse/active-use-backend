﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Core.Sql
{
    using System.Configuration;
    using System.Data.SqlClient;

    public abstract class BaseDataAccess
    {
        protected abstract string ConnectionStringName { get; set; }

        /// <summary>
        /// Executes MsSQL query with result.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="query">The query text.</param>
        /// <param name="readerFunc">The reader function.</param>
        /// <param name="commandParamsSetter">The command parameters setter.</param>
        /// <returns>
        ///   <see cref="TResult" />
        /// </returns>
        protected async Task<TResult> RunMsSqlQuery<TResult>(
            string query,
            Func<SqlCommand, Task<TResult>> readerFunc,
            Action<SqlParameterCollection> commandParamsSetter)
        {
            TResult result;

            using (var connection = await SqlFactory.CreateConnection(ConfigurationManager.ConnectionStrings[this.ConnectionStringName].ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.CommandTimeout = 600;
                    commandParamsSetter?.Invoke(command.Parameters);
                    result = await readerFunc(command);
                }
            }

            return result;
        }
    }
}
