﻿namespace Service.Core
{
    public interface BaseIdentity<TKey> where TKey : struct
    {
        TKey Id { get; set; }
    }
}