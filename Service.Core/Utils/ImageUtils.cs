﻿namespace Service.Core.Utils
{
    using System;
    using System.Drawing;

    public static class ImageUtils
    {
        public static Size ResizeKeepAspect(Image image, int maxWidth, int maxHeight)
        {
            var newHeight = image.Height;
            var newWidth = image.Width;
            if (maxWidth > 0 && newWidth > maxWidth)
            {
                var divider = Math.Abs(newWidth / maxWidth);
                newWidth = maxWidth;
                newHeight = (int)Math.Round((float)(newHeight / divider));
            }

            if (maxHeight > 0 && newHeight > maxHeight)
            {
                var divider = Math.Abs(newHeight / maxHeight);
                newHeight = maxHeight;
                newWidth = (int)Math.Round((float)(newWidth / divider));
            }

            return new Size(newWidth, newHeight);
        }
    }
}