﻿namespace Service.Core.Utils
{
    using System.Linq;

    using Service.Core.Entities.Enums;

    public static class ImageExtensionValidator
    {
        public static ImageExtension FindExtension(byte[] data)
        {
            if (data.Take(3).SequenceEqual(new byte[] { 0xFF, 0xD8, 0xFF }))
            {
                return ImageExtension.Jpg;
            }

            if (data.Take(2).SequenceEqual(new byte[] { 0x42, 0x4D }))
            {
                return ImageExtension.Bmp;
            }

            if (data.Skip(1).Take(3).SequenceEqual(new byte[] { 0x50, 0x4E, 0x47 }))
            {
                return ImageExtension.Png;
            }

            return ImageExtension.Unknown;
        }
    }

}
