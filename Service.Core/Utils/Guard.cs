﻿namespace Service.Core.Utils
{
    using System;

    public sealed class Guard
    {
        public static void ArgumentNotNull(object arg, string name)
        {
            if (arg == null)
            {
                throw new ArgumentNullException(name);
            }
        }
    }
}