﻿namespace Service.Core.Caching
{
    using System;
    using System.Runtime.Caching;

    public sealed class SimpleCache : ISimpleCache
    {
        private readonly ObjectCache cache;

        private readonly CacheItemPolicy hitPolicy;

        private readonly CacheItemPolicy missPolicy;

        public SimpleCache()
        {
            this.cache = MemoryCache.Default;
            this.hitPolicy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) };
            this.missPolicy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(5) };
        }

        public void Add(string key, object obj)
        {
            if (obj != null)
            {
                this.cache.Add(key, obj, this.hitPolicy);
            }
        }

        public object Get(string key)
        {
            return this.cache.Get(key);
        }

        public void Invalidate(string key)
        {
            this.cache.Remove(key);
        }

        public void Update(string key, object obj)
        {
            if (obj != null)
            {
                this.Invalidate(key);
                this.Add(key, obj);
            }
        }
    }
}