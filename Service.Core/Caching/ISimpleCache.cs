﻿namespace Service.Core.Caching
{
    public interface ISimpleCache
    {
        void Add(string key, object obj);

        object Get(string key);

        void Invalidate(string key);

        void Update(string key, object obj);
    }
}