﻿namespace Service.Core.Cloud
{
    using System;
    using System.IO;

    /// <summary>
    /// BLOB implementation
    /// </summary>
    public class StreamBlob : IBlob
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StreamBlob"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="name">The name.</param>
        public StreamBlob(Stream stream, string name)
        {
            this.Stream = stream;
            this.Name = name;
            this.MaxAge = TimeSpan.FromDays(20 * 365);
        }

        /// <inheritdoc />
        public TimeSpan MaxAge { get; }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public Stream Stream { get; }

        /// <inheritdoc />
        public Uri Url => null;
    }
}