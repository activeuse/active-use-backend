﻿namespace Service.Core.Cloud
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;

    internal sealed class AzureBlobStorageService : IAzureBlobStorageService
    {
        /// <summary>
        /// The BLOB container
        /// </summary>
        private readonly Lazy<CloudBlobContainer> blobContainer;

        /// <summary>
        /// The connection string
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureBlobStorageService" /> class.
        /// </summary>
        public AzureBlobStorageService()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["Azure"].ConnectionString;
            this.blobContainer = new Lazy<CloudBlobContainer>(() => this.GetBlobContainer(ConfigurationManager.AppSettings["ItemPhotosBlobContainer"]));
        }

        /// <inheritdoc />
        public async Task<string> Upload(Stream image, string blobName)
        {
            var url = await this.SaveBlobInternal(new StreamBlob(image, blobName));
            return url;
        }

        /// <summary>
        /// Gets the BLOB container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <returns><see cref="CloudBlobContainer"/></returns>
        private CloudBlobContainer GetBlobContainer(string containerName)
        {
            if (CloudStorageAccount.TryParse(this.connectionString, out var storageAccount))
            {
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference(containerName);
                if (container.CreateIfNotExists())
                {
                    container.SetPermissions(
                        new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }

                return container;
            }

            return null;
        }

        /// <summary>
        /// Saves the BLOB to Azure.
        /// </summary>
        /// <param name="blob">The BLOB.</param>
        /// <returns><see cref="Task"/></returns>
        private async Task<string> SaveBlobInternal(IBlob blob)
        {
            var blockBlob = this.blobContainer.Value.GetBlockBlobReference(blob.Name);
            blockBlob.Properties.CacheControl = $"max-age={blob.MaxAge.TotalSeconds}, must-revalidate";

            using (var stream = blob.Stream)
            {
                // If the blob already exists, it will be overwritten.
                await blockBlob.UploadFromStreamAsync(stream);
            }

            return blockBlob.Uri.ToString();
        }

        /// <summary>
        /// Deletes the specified file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns><see cref="Task"/></returns>
        public async Task Delete(string fileName)
        {
            var blob = this.blobContainer.Value.GetBlockBlobReference(fileName);
            await blob.DeleteIfExistsAsync();
        }
    }
}