﻿namespace Service.Core.Cloud
{
    using System.IO;
    using System.Threading.Tasks;

    public interface IAzureBlobStorageService
    {
        Task<string> Upload(Stream image, string blobName);

        Task Delete(string fileName);
    }
}