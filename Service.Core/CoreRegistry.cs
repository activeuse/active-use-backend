﻿namespace Service.Core
{
    using Service.Core.Caching;
    using Service.Core.Cloud;
    using Service.Core.Logging;
    using Service.Core.Security;

    using StructureMap;

    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            this.For<IAzureBlobStorageService>().Use<AzureBlobStorageService>();
            this.For<IDatabaseLogger>().Use<DatabaseLogger>();
            this.For<ISecretHelper>().Use<SecretHelper>();
            this.For<ISimpleCache>().Use<SimpleCache>();
        }
    }
}