﻿namespace Service.Core.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RentItemFeatures")]
    public class RentItemFeatureEntity
    {
        public string Id { get; set; }

        public ICollection<RentItemFeatureItem> Items { get; set; }

        public string Name { get; set; }
    }
}