﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    [Table("ItemPhotos")]
    public class ItemPhotoEntity : BaseIdentity<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string CloudUrl { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public bool IsThumbnail { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }
    }
}