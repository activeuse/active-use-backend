﻿namespace Service.Core.Entities
{
    using System;

    public interface IDatedEntity
    {
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }
    }
}