﻿namespace Service.Core.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics;

    using Newtonsoft.Json;

    [Table("RentItems")]
    [DebuggerDisplay("Name: {Name}, Photos: {Photos.Count}, Booking Requests: {BookingRequests.Count}")]
    public class RentItemEntity
    {
        [Key]
        [ForeignKey("Advertisement")]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Details { get; set; }

        public int? GenderId { get; set; }

        public ItemCategoryEntity ItemCategory { get; set; }

        public int ItemCategoryId { get; set; }

        public ItemDimensionsEntity Dimensions { get; set; }

        public GenderEntity Gender { get; set; }

        [JsonIgnore]
        public AdvertisementEntity Advertisement { get; set; }

        public ICollection<ItemPackageEntity> Package { get; set; }

        public ICollection<RentItemPropertyEntity> Properties { get; set; }

        public ICollection<ItemPhotoEntity> Photos { get; set; }

        public ICollection<ItemDeliveryEntity> Deliveries { get; set; }

        public ICollection<BookingRequestEntity> BookingRequests { get; set; }
    }
}