﻿namespace Service.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics;

    [Table("BookingRequests")]
    [DebuggerDisplay("Start: {StartDateTime}, End: {EndDateTime}")]
    public class BookingRequestEntity : IDatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("RentItemId")]
        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }

        public DateTime EndDateTime { get; set; }

        public DateTime StartDateTime { get; set; }

        public string RequesterUserId { get; set; }

        public int StatusId { get; set; }

        public string SecretPhrase { get; set; }

        public BookingRequestStatusEntity Status { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}