﻿namespace Service.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    using Service.Core.Entities.Enums;

    [Table("ItemWeights")]
    public sealed class ItemDimensionsEntity
    {
        [Key]
        [ForeignKey("RentItem")]
        public int Id { get; set; }

        public float? Depth { get; set; }

        public float Height { get; set; }

        public float? Weight { get; set; }

        public float Width { get; set; }

        public int MeasureUnitId { get; set; }

        public MeasureUnitEntity MeasureUnit { get; set; }

        [JsonIgnore]
        public RentItemEntity RentItem { get; set; }
    }
}