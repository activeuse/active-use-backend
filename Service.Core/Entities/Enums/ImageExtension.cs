﻿namespace Service.Core.Entities.Enums
{
    public enum ImageExtension
    {
        Jpg,

        Bmp,

        Png,

        Unknown
    }
}