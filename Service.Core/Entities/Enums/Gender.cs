﻿namespace Service.Core.Entities.Enums
{
    public enum Gender
    {
        Female,

        Male,

        Boy,

        Girl,

        Unisex
    }
}