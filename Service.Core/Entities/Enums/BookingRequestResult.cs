﻿namespace Service.Core.Entities.Enums
{
    public enum BookingRequestResult
    {
        Success,

        AlreadyExists,

        MaxNumberOfActiveRequestsExceeded,

        DatesNotAvailable,

        CantBookOwnItem,

        InsufficientBalance,

         AdvertisementIsNotActive,

         AdvertisementDoesNotExist
    }
}