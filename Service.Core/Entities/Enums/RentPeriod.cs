﻿namespace Service.Core.Entities.Enums
{
    public enum RentPeriod
    {
        Hour,

        Day,

        Month
    }
}