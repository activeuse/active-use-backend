﻿namespace Service.Core.Entities.Enums
{
    public enum ItemCategory
    {
        Vehicles,

        Property,

        Personal,

        Electronics,

        Hobby,

        Animals,

        ForBusiness
    }
}