﻿namespace Service.Core.Entities.Enums
{
    public enum DeliveryType
    {
        Pickup,

        Cargo,

        HeavyWeightCargo 
    }
}