﻿namespace Service.Core.Entities.Enums
{
    public enum AdvertisementStatus
    {
        New,

        InModeration,

        RejectedByModerator,

        Active,

        Expired,

        Deleted
    }
}