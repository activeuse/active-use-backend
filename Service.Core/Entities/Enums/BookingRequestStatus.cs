﻿namespace Service.Core.Entities.Enums
{
    public enum BookingRequestStatus
    {
        New,

        Deleted,

        Rejected,

        Approved,

        Completed
    }
}