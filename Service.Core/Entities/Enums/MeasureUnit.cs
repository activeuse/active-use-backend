﻿namespace Service.Core.Entities.Enums
{
    public enum MeasureUnit
    {
        Millimeter,

        Centimeter,

        Meter,

        Inch
    }
}