﻿namespace Service.Core.Entities.Enums
{
    public enum Currency
    {
        USD,

        Euro,

        Rouble,

        EVT
    }
}