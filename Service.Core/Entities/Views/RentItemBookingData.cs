﻿namespace Service.Core.Entities.Views
{
    using System;

    public class RentItemBookingData
    {
        public int AdvertisementId { get; set; }

        public string AdvertisementOwnerUserId { get; set; }

        public string AdvertisementTitle { get; set; }

        public int BookingRequestId { get; set; }

        public int BookingRequestStatus { get; set; }

        public DateTime EndDateTime { get; set; }

        public decimal FullPricePerPeriod { get; set; }

        public int ItemCategoryId { get; set; }

        public string RentItemName { get; set; }

        public string RequesterUserId { get; set; }

        public DateTime StartDateTime { get; set; }

        public string SecretPhrase { get; set; }
    }
}