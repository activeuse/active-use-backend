﻿namespace Service.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Bookings")]
    public class BookingEntity : IDatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public BookingRequestEntity BookingRequest { get; set; }

        public int BookingRequestId { get; set; }

        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}