﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Service.Core.Entities.Enums;

    [Table("ItemCategories")]
    public class ItemCategoryEntity : BaseIdentity<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}