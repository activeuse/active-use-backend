﻿namespace Service.Core.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Countries")]
    public class CountryEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Native { get; set; }

        public string Phone { get; set; }

        public string Currency { get; set; }

        public ICollection<CityEntity> Cities { get; set; }
    }
}