﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("AdvertisementLocations")]
    public sealed class LocationEntity
    {
        [Key]
        [ForeignKey("Advertisement")]
        public int Id { get; set; }

        public string Address { get; set; }

        public int CityId { get; set; }

        public CityEntity City { get; set; }

        public AdvertisementEntity Advertisement { get; set; }
    }
}