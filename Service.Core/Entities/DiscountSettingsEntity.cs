﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    [Table("DiscountSettings")]
    public class DiscountSettingsEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonIgnore]
        public RentConfigurationEntity RentConfiguration { get; set; }

        public int RentConfigurationId { get; set; }

        public int FromInclusive { get; set; }

        public int ToInclusive { get; set; }

        public float Discount { get; set; }
    }
}