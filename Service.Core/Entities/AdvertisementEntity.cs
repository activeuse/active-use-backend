﻿namespace Service.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Diagnostics;

    [Table("Advertisements")]
    [DebuggerDisplay("Title: {Title}, UserId: {UserId}, Status: {StatusId}")]
    public class AdvertisementEntity : IDatedEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public AdvertisementStatusEntity Status { get; set; }

        public RentItemEntity RentItem { get; set; }

        public ICollection<RentConfigurationEntity> RentConfiguration { get; set; }

        public LocationEntity Location { get; set; }

        public string UserId { get; set; }

        public int StatusId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}