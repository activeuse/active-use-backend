﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CoverPhotos")]
    public class CoverPhotoEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ItemPhotoId { get; set; }

        [ForeignKey("ItemPhotoId")]
        public ItemPhotoEntity ItemPhoto { get; set; }

        public int AdvertisementId { get; set; }

        [ForeignKey("AdvertisementId")]
        public AdvertisementEntity Advertisement { get; set; }
    }
}