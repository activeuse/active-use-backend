﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Tags")]
    public sealed class TagEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}