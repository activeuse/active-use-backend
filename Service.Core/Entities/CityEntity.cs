﻿namespace Service.Core.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Cities")]
    public class CityEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public CountryEntity Country { get; set; }

        public int CountryId { get; set; }
    }
}