﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    [Table("ItemDeliveries")]
    public class ItemDeliveryEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DeliveryTypeEntity DeliveryType { get; set; }

        public int DeliveryTypeId { get; set; }

        [JsonIgnore]
        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }
    }
}