﻿namespace Service.Core.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    using Service.Core.Entities.Enums;

    [Table("RentConfigurations")]
    public class RentConfigurationEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonIgnore]
        public AdvertisementEntity Advertisement { get; set; }

        public int AdvertisementId { get; set; }

        public ICollection<DiscountSettingsEntity> DiscountSettings { get; set; }

        public decimal FullPricePerPeriod { get; set; }

        public int? MaxRentDuration { get; set; }

        public int? MinRentDuration { get; set; }

        public RentPeriodEntity RentPeriod { get; set; }

        public int RentPeriodId { get; set; }

        public CurrencyEntity Currency { get; set; }

        public int CurrencyId { get; set; }
    }
}