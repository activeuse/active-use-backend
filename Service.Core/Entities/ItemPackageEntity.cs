﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    [Table("ItemPackages")]
    public class ItemPackageEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ObjectName { get; set; }

        public int Quantity { get; set; }

        [JsonIgnore]
        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }
    }
}