﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RentItemFeatureItem")]
    public class RentItemFeatureItemEntities
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}