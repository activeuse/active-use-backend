﻿namespace Service.Core.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    [Table("RentItemProperties")]
    public sealed class RentItemPropertyEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        [JsonIgnore]
        public RentItemEntity RentItem { get; set; }

        public int RentItemId { get; set; }
    }
}