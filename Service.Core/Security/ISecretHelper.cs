﻿namespace Service.Core.Security
{
    public interface ISecretHelper
    {
        string Decrypt(string encryptedText);

        string Encrypt(string plainText);
    }
}