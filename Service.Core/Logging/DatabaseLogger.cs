﻿namespace Service.Core.Logging
{
    using System;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    internal sealed class DatabaseLogger : IDatabaseLogger
    {
        public async Task LogEvent(LogType logType, object context)
        {
            var data = JsonConvert.SerializeObject(context);
            const string Query = "insert into [EventLog] values (@data, @date, @logType, @host);";

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Events"].ConnectionString))
            {
                await connection.OpenAsync();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("logType", (int)logType);
                    command.Parameters.AddWithValue("data", data);
                    command.Parameters.AddWithValue("date", DateTime.UtcNow);
                    command.Parameters.AddWithValue("host", Environment.MachineName);

                    await command.ExecuteNonQueryAsync();
                }
            }
        }
    }
}