﻿namespace Service.Core.Logging
{
    using System.Threading.Tasks;

    public interface IDatabaseLogger
    {
        Task LogEvent(LogType logType, object context);
    }
}