﻿namespace Service.Core.Logging
{
    public enum LogType
    {
        Info,
        Warning,
        Error,
        CriticalError,
        NetworkError,
        Create,
        Upload,
        Update,
        Delete,
        StatusChange,
    }
}
