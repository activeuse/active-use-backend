﻿namespace Authentication.Service.Models
{
    using System.ComponentModel.DataAnnotations;

    public class UpdatePasswordModel
    {
        [Required]
        [MinLength(6)]
        [MaxLength(64)]
        public string NewPassword { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(64)]
        public string NewPasswordRepeated { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(64)]
        public string OldPassword { get; set; }
    }
}