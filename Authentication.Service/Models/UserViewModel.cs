﻿namespace Authentication.Service.Models
{
    public class UserViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string PhoneNumber { get; set; }
    }
}