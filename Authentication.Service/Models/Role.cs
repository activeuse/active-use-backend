﻿namespace Authentication.Service.Models
{
    public enum Role
    {
        User,

        Developer,

        Administrator
    }
}