﻿namespace Authentication.Service.Models
{
    using System.ComponentModel.DataAnnotations;

    public class UserUpdateModel
    {
        [Required]
        [MaxLength(255)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(255)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(16)]
        public string PhoneNumber { get; set; }
    }
}