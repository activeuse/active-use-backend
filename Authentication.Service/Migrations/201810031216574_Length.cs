namespace Authentication.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Length : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "Password", c => c.String(nullable: false, maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "Password", c => c.String(nullable: false));
        }
    }
}
