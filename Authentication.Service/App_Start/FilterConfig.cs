﻿namespace Authentication.Service
{
    using System.Web.Mvc;

    using Authentication.Service.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}