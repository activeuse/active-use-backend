﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(Authentication.Service.App_Start.Startup))]

namespace Authentication.Service.App_Start
{
    using System;
    using System.Web.Http;

    using Authentication.Service.Infrastructure;

    using Microsoft.Owin.Security.OAuth;

    using Owin;

    using WebApi.StructureMap;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            SwaggerConfig.Register(config);
            this.ConfigureOAuth(app);

            config.UseStructureMap(
                x =>
                    {
                        x.AddRegistry<AuthServiceRegistry>();
                    });

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions
                                         {
                                             AllowInsecureHttp = true,
                                             TokenEndpointPath =
                                                 new PathString("/token"),
                                             AccessTokenExpireTimeSpan =
                                                 TimeSpan.FromDays(1),
                                             Provider =
                                                 new SimpleAuthorizationServerProvider()
                                         };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}