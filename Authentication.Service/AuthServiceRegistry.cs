﻿namespace Authentication.Service
{
    using Authentication.Service.Infrastructure;

    using StructureMap;

    public class AuthServiceRegistry : Registry
    {
        public AuthServiceRegistry()
        {
            this.For<IUsersRepository>().Use<UsersRepository>();
        }
    }
}