﻿namespace Authentication.Service.Infrastructure
{
    using System;
    using System.Threading.Tasks;

    using Authentication.Service.Models;

    using Microsoft.AspNet.Identity;

    public interface IUsersRepository : IDisposable
    {
        Task<UserModel> FindUser(string userName, string password);

        Task<UserModel> FindUser(string name);

        Task<IdentityResult> RegisterUser(UserModel userModel, Role[] roles);

        Task Update(string userName, UserUpdateModel model);

        Task<bool> UpdatePassword(string userName, string currentPassword, string newPassword);
    }
}