﻿namespace Authentication.Service.Infrastructure
{
    using Authentication.Service.Models;

    using Microsoft.AspNet.Identity.EntityFramework;

    public class AuthContext : IdentityDbContext<UserModel>
    {
        public AuthContext()
            : base("AuthContext")
        {
        }
    }
}