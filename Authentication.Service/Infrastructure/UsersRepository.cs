﻿namespace Authentication.Service.Infrastructure
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Authentication.Service.Models;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class UsersRepository : IUsersRepository
    {
        private readonly AuthContext ctx;

        private readonly UserManager<UserModel> userManager;

        private readonly RoleManager<IdentityRole> roleManager;

        public UsersRepository()
        {
            this.ctx = new AuthContext();
            this.userManager = new UserManager<UserModel>(new UserStore<UserModel>(this.ctx));
            this.roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new AuthContext()));
        }

        public void Dispose()
        {
            this.ctx.Dispose();
            this.userManager.Dispose();
        }

        public async Task<UserModel> FindUser(string userName, string password)
        {
            var user = await this.userManager.FindAsync(userName, password);
            if (user == null)
            {
                return user;
            }

            var role = await this.roleManager.FindByIdAsync(user.Roles.First().RoleId);

            user.Role = role.Name;
            return user;
        }

        public async Task Update(string userName, UserUpdateModel model)
        {
            var user = await this.userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return;
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;

            if (user.PhoneNumber != model.PhoneNumber)
            {
                user.PhoneNumber = model.PhoneNumber;
                user.PhoneNumberConfirmed = false;
            }

            await this.userManager.UpdateAsync(user);
        }

        public async Task<bool> UpdatePassword(string userName, string currentPassword, string newPassword)
        {
            var user = await this.userManager.FindAsync(userName, currentPassword);
            if (user == null)
            {
                return false;
            }

            var result = await this.userManager.ChangePasswordAsync(user.Id, currentPassword, newPassword);
            return result.Succeeded;
        }

        public async Task<UserModel> FindUser(string name)
        {
            var user = await this.userManager.FindByNameAsync(name);
            var role = await this.roleManager.FindByIdAsync(user.Roles.First().RoleId);

            user.Role = role.Name;
            return user;
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel, Role[] roles)
        {
            userModel.UserName = userModel.Email;
            var result = await this.userManager.CreateAsync(userModel, userModel.Password);
            if (result.Succeeded)
            {
                if (roles != null && roles.Any())
                {
                    foreach (var role in roles)
                    {
                        if (!await this.roleManager.RoleExistsAsync(role.ToString()))
                        {
                            await this.roleManager.CreateAsync(new IdentityRole(role.ToString()));
                        }
                    }

                    result = await this.userManager.AddToRolesAsync(userModel.Id, roles.Select(x => x.ToString()).ToArray());
                }
            }

            return result;
        }
    }
}