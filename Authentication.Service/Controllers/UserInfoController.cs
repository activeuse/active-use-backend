﻿namespace Authentication.Service.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Authentication.Service.Filters;
    using Authentication.Service.Infrastructure;
    using Authentication.Service.Models;

    using Microsoft.AspNet.Identity;

    [Authorize(Roles = "Developer,User")]
    [RoutePrefix("api/userInfo")]
    public class UserInfoController : ApiController
    {
        private readonly IUsersRepository usersRepository;

        public UserInfoController(IUsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
        }

        /// <summary>
        /// Gets basic information about user.
        /// </summary>
        /// <returns>User info.</returns>
        [HttpGet]
        public async Task<UserViewModel> Get()
        {
            var principal = this.RequestContext.Principal;
            var userName = principal.Identity.GetUserName();
            using (this.usersRepository)
            {
                var user = await this.usersRepository.FindUser(userName);
                return new UserViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Login = user.UserName,
                    PhoneNumber = user.PhoneNumber
                };
            }
        }

        /// <summary>
        /// Updates user info.
        /// </summary>
        /// <param name="model">The update user model.</param>
        /// <returns><see cref="Task"/></returns>
        [HttpPut]
        [ValidateModelState]
        public async Task Update(UserUpdateModel model)
        {
            var principal = this.RequestContext.Principal;
            var userName = principal.Identity.GetUserName();

            using (this.usersRepository)
            {
                await this.usersRepository.Update(userName, model);
            }
        }

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="model">The password update model.</param>
        /// <returns>Password update result.</returns>
        [HttpPut]
        [Route("password")]
        public async Task<HttpResponseMessage> UpdatePassword(UpdatePasswordModel model)
        {
            if (model.NewPassword == model.OldPassword)
            {
                return this.Request.CreateResponse(HttpStatusCode.Conflict, "Old password matches new one.");
            }

            if (model.NewPassword != model.NewPasswordRepeated)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.Conflict, "Passwords do not match.");
            }

            var principal = this.RequestContext.Principal;
            var userName = principal.Identity.GetUserName();

            using (this.usersRepository)
            {
                var result = await this.usersRepository.UpdatePassword(userName, model.OldPassword, model.NewPassword);
                if (result)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK);
                }

                return this.Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Invalid password.");
            }
        }
    }
}