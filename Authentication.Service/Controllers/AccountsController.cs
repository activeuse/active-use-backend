﻿namespace Authentication.Service.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Http;

    using Authentication.Service.Filters;
    using Authentication.Service.Infrastructure;
    using Authentication.Service.Models;

    using Microsoft.AspNet.Identity;

    public class AccountsController : ApiController
    {
        private readonly IUsersRepository usersRepository;

        public AccountsController(IUsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
        }

        /// <summary>
        /// Registers the user.
        /// </summary>
        /// <param name="userModel">The user model.</param>
        /// <returns>Result of registration.</returns>
        [AllowAnonymous]
        [HttpPost]
        [ValidateModelState]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            var result = await this.usersRepository.RegisterUser(userModel, new[] { Role.Developer, Role.User });

            var errorResult = this.GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return this.Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.usersRepository.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return this.InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error);
                    }
                }

                if (this.ModelState.IsValid)
                {
                    return this.BadRequest();
                }

                return this.BadRequest(this.ModelState);
            }

            return null;
        }
    }
}