﻿namespace Bookings.Service.Models.Booking
{
    using System;

    public class BookingRequest
    {
        public DateTime EndDate { get; set; }

        public float Quantity { get; set; }

        public int RentItemId { get; set; }

        public int RentPeriodId { get; set; }

        public DateTime StartDate { get; set; }
    }
}