﻿namespace Bookings.Service.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Http;

    using Bookings.Service.Filters;
    using Bookings.Service.Models.Booking;

    [JwtAuthentication]
    public class BookingsController : BaseIdentityController
    {
        private readonly IBookingService bookingService;

        public BookingsController(IBookingService bookingService)
        {
            this.bookingService = bookingService;
        }

        [HttpPost]
        public async Task Request(BookingRequest request)
        {
            await this.bookingService.Request(
                new BookingRequestInfo
                    {
                        RentItemId = request.RentItemId,
                        StartDate = request.StartDate,
                        EndDate = request.EndDate,
                        RequesterUserId = this.GetUserId(),
                    });
        }
    }
}