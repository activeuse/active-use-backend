﻿namespace Bookings.Service.Controllers
{
    using System.Web.Http;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// Base controller for operations which require authorization.
    /// </summary>
    public class BaseIdentityController : ApiController
    {
        /// <summary>
        /// Gets ID of authorized user.
        /// </summary>
        /// <returns>User's ID.</returns>
        protected string GetUserId()
        {
            return this.User.Identity.GetUserId();
        }
    }
}