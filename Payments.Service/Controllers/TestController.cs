﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payments.Service.Controllers
{
    using System.Web.Http;
    public class TestController : ApiController
    {
        [HttpGet]
        public int Test()
        {
            return 1;
        }
    }
}