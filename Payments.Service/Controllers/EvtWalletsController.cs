﻿namespace Payments.Service.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Payments.Service.DataAccess.Exceptions;
    using Payments.Service.DataAccess.Services;
    using Payments.Service.Filters;
    using Payments.Service.Models;

    [JwtAuthentication]
    [RoutePrefix("api/evtWallets")]
    public class EvtWalletsController : BaseIdentityController
    {
        private readonly IEvtWalletsService evtWalletsService;

        public EvtWalletsController(IEvtWalletsService evtWalletsService)
        {
            this.evtWalletsService = evtWalletsService;
        }

        [HttpGet]
        public async Task<IEnumerable<EvtWalletViewModel>> GetAllWallets()
        {
            var userId = this.GetUserId();
            var wallets = await this.evtWalletsService.GetAllWallets(userId);
            return wallets.Select(
                x => new EvtWalletViewModel
                {
                    Id = x.Id,
                    Address = x.Address,
                    CreatedAt = x.CreatedAt,
                    IsActive = x.IsActive,
                    Balance = x.Balance
                });
        }

        [HttpPut]
        [Route("activate")]
        public async Task Activate([FromUri]int walletId)
        {
            var userId = this.GetUserId();
            await this.evtWalletsService.Activate(walletId, userId);
        }

        [HttpPut]
        [Route("deactivate")]
        public async Task Deactivate([FromUri]int walletId)
        {
            var userId = this.GetUserId();
            await this.evtWalletsService.Deactivate(walletId, userId);
        }

        [HttpPost]
        [ValidateModelState]
        public async Task<HttpResponseMessage> CreateWallet(CreateEvtWalletModel createModel)
        {
            var userId = this.GetUserId();
            int walletId;
            try
            {
                walletId = await this.evtWalletsService.CreateWallet(userId, createModel.SecretPhrase);
            }
            catch (WalletsCountExceededException ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Wallets count exceeded");
            }
            catch (WalletsCreationException ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Wallet creation failed");
            }
            catch (WebException ex)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.ServiceUnavailable, "EVT wallet service is under maintenance");
            }

            return this.Request.CreateResponse(HttpStatusCode.OK, walletId);
        }
    }
}