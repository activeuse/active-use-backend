﻿namespace Payments.Service.Models
{
    using System;

    public class EvtWalletViewModel
    {
        public string Address { get; set; }

        public DateTime CreatedAt { get; set; }

        public decimal Balance { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }
    }
}