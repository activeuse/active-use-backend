﻿namespace Payments.Service.Models
{
    using System.ComponentModel.DataAnnotations;

    public class CreateEvtWalletModel
    {
        [Required]
        [MinLength(20)]
        public string SecretPhrase { get; set; }
    }
}