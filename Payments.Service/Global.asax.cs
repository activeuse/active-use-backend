﻿namespace Payments.Service
{
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;
    using System.Web.Mvc;
    using System.Web.Routing;

    using global::Service.Core;

    using Payments.Service.DataAccess;
    using Payments.Service.Filters;

    using WebApi.StructureMap;

    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());
            GlobalConfiguration.Configuration.UseStructureMap(
                x =>
                    {
                        x.AddRegistry<CoreRegistry>();
                        x.AddRegistry<PaymentsServiceRegistry>();
                    });

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(
                new QueryStringMapping("type", "json", new MediaTypeHeaderValue("application/json")));
        }
    }
}