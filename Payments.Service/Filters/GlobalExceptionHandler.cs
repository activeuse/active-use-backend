﻿namespace Payments.Service.Filters
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.ExceptionHandling;
    using System.Web.Http.Results;

    using global::Service.Core.Exceptions;

    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var md = new Metadata
            {
                Message = context.Exception.Message,
                DateTime = DateTime.Now,
                RequestUri = context.Request.RequestUri.ToString()
            };

            switch (context.Exception)
            {
                case ItemNotFoundException ex:
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.BadRequest, md));
                    break;

                case EntityBadStateException ex:
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.BadRequest, md));
                    break;
                default:
                    context.Result = new ResponseMessageResult(context.Request.CreateResponse(HttpStatusCode.InternalServerError, md));
                    break;
            }
        }

        private class Metadata
        {
            public string Message { get; set; }

            public DateTime DateTime { get; set; }

            public string RequestUri { get; set; }
        }
    }
}