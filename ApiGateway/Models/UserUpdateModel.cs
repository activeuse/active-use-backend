﻿namespace ApiGateway.Models
{
    using System.ComponentModel.DataAnnotations;

    public class UserUpdateModel
    {
        [Required]
        [MaxLength(255)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(255)]
        public string LastName { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}