﻿namespace ApiGateway.Models
{
    using Newtonsoft.Json;

    public class AccessTokenViewModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("evt_wallet_exists")]
        public bool EvtWalletExists { get; set; }
    }
}