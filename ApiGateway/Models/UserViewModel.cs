﻿namespace ApiGateway.Models
{
    public class UserViewModel
    {
        public bool EvtWalletExists { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string PhoneNumber { get; set; }
    }
}