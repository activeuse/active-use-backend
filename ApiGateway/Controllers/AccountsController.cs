﻿namespace ApiGateway.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    using ApiGateway.Models;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;

    [Route("api/[controller]/[action]")]
    public class AccountsController : Controller
    {
        private readonly IConfiguration configuration;

        private readonly SignInManager<UserModel> signInManager;

        private readonly RoleManager<IdentityRole> roleManager;

        private readonly UserManager<UserModel> userManager;

        public AccountsController(
            UserManager<UserModel> userManager,
            SignInManager<UserModel> signInManager,
            IConfiguration configuration,
            RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.roleManager = roleManager;
        }

        [HttpPost]
        [HttpOptions]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = this.userManager.Users.SingleOrDefault(r => r.Email == model.Email);
                if (appUser != null)
                {
                    var token = this.GenerateJwtToken(model.Email, appUser);
                    var tokenModel = new AccessTokenViewModel
                    {
                        AccessToken = token,
                        TokenType = "Bearer",
                        ExpiresIn = int.Parse(this.configuration["JwtExpireDays"]) * 24 * 60 * 60,
                        EvtWalletExists = appUser.EvtWalletExists
                    };
                    return this.Ok(tokenModel);
                }
            }

            return this.Unauthorized();
        }

        [HttpPost]
        [HttpOptions]
        public async Task<object> Register([FromBody] RegisterModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var user = new UserModel
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumberConfirmed = false,
                EmailConfirmed = false,
                EvtWalletExists = false
            };

            var result = await this.userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return this.BadRequest(result.Errors);
            }

            if (!await this.roleManager.RoleExistsAsync("User"))
            {
                await this.roleManager.CreateAsync(new IdentityRole("User"));
            }

            if (result.Succeeded)
            {
                result = await this.userManager.AddToRoleAsync(user, "User");

                if (result.Succeeded)
                {
                    await this.signInManager.SignInAsync(user, false);
                    var token = this.GenerateJwtToken(model.Email, user);
                    return new AccessTokenViewModel
                    {
                        AccessToken = token,
                        TokenType = "Bearer",
                        ExpiresIn = int.Parse(this.configuration["JwtExpireDays"]) * 24 * 60 * 60
                    };
                }
            }

            throw new ApplicationException("UNKNOWN_ERROR");
        }

        private string GenerateJwtToken(string email, IdentityUser user)
        {
            var claims = new List<Claim>
                             {
                                 new Claim(ClaimTypes.NameIdentifier, user.Id),
                                 new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                             };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(this.configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                this.configuration["JwtIssuer"],
                this.configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}