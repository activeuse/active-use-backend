﻿namespace ApiGateway.Controllers
{
    using System.Threading.Tasks;

    using ApiGateway.Models;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    [Authorize]
    public class UserInfoController : Controller
    {
        private readonly UserManager<UserModel> userManager;

        public UserInfoController(UserManager<UserModel> userManager)
        {
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<UserViewModel> Get()
        {
            var user = await this.userManager.GetUserAsync(this.User);
            return new UserViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Login = user.UserName,
                PhoneNumber = user.PhoneNumber,
                EvtWalletExists = user.EvtWalletExists
            };
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UserUpdateModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var user = await this.userManager.GetUserAsync(this.User);
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.PhoneNumber = model.PhoneNumber;

            await this.userManager.UpdateAsync(user);

            return this.Ok();
        }
    }
}