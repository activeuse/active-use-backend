﻿namespace ApiGateway
{
    using System.IdentityModel.Tokens.Jwt;
    using System.IO;

    using ApiGateway.Auth;
    using ApiGateway.Models;

    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;

    using Ocelot.DependencyInjection;
    using Ocelot.Middleware;

    public class Program
    {
        public static void Main(string[] args)
        {
            new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration(
                (hostingContext, config) =>
                    {
                        var env = hostingContext.HostingEnvironment.EnvironmentName;

                        config.SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                            .AddJsonFile("appsettings.json", true, true)
                            .AddJsonFile($"appsettings.{env}.json", true, true)
                            .AddJsonFile(Path.Combine("configuration", $"configuration.{env}.json")).AddEnvironmentVariables();
                    })
                .ConfigureLogging(
                (hostingContext, logging) =>
                    {
                    })
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}