﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiGateway.Migrations
{
    public partial class WalletInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EvtWalletExists",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvtWalletExists",
                table: "AspNetUsers");
        }
    }
}
