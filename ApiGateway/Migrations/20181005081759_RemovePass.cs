﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiGateway.Migrations
{
    public partial class RemovePass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "AspNetUsers",
                maxLength: 64,
                nullable: false,
                defaultValue: "");
        }
    }
}
