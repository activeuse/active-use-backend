﻿namespace ApiGateway.Auth
{
    using ApiGateway.Models;

    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    public class AuthContext : IdentityDbContext<UserModel>
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());
        }

        private static string GetConnectionString()
        {
            const string DatabaseName = "ActiveUseDev";

            const string DatabaseUser = "developer";

            const string DatabasePass = "<%ZtLKMFGJdw";

            return $"Server=185.243.131.54;" + $"database={DatabaseName};" + $"uid={DatabaseUser};" + $"pwd={DatabasePass};" + $"pooling=true;";
        }
    }
}