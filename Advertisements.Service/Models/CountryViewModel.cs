﻿namespace Advertisements.Service.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string NativeCountryName { get; set; }

        public string Phone { get; set; }

        public string Currency { get; set; }
    }
}