﻿namespace Advertisements.Service.Models.Booking
{
    using System;

    using global::Service.Core.Entities.Enums;

    public class BookingRequest
    {
        public DateTime EndDate { get; set; }

        public int RentItemId { get; set; }

        public DateTime StartDate { get; set; }

        public string SecretPhrase { get; set; }
    }
}