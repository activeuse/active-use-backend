﻿namespace Advertisements.Service.Models.Booking
{
    using System;

    using Advertisements.Service.Models.Advertisement.ViewModels;
    using Advertisements.Service.Models.General;

    public class BookingRequestViewModel
    {
        public AdvertisementInfoViewModel AdvertisementInfo { get; set; }

        public DateTime EndDate { get; set; }

        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public KeyValue Status { get; set; }
    }
}