﻿namespace Advertisements.Service.Models
{
    using System.ComponentModel.DataAnnotations;

    public class AdvertisementSearchModel
    {
        public int? CategoryId { get; set; }

        [MaxLength(255)]
        public string Pattern { get; set; }

        public int? CityId { get; set; }
    }
}