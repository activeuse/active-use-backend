﻿namespace Advertisements.Service.Models.Advertisement.UpdateModels
{
    public class PhotoViewModel
    {
        public int Id { get; set; }

        public int Height { get; set; }

        public bool IsThumbnail { get; set; }

        public string Url { get; set; }

        public int Width { get; set; }

        public string Name { get; set; }
    }
}