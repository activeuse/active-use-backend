﻿namespace Advertisements.Service.Models.Advertisement.UpdateModels
{
    using System.ComponentModel.DataAnnotations;

    public class LocationUpdateModel
    {
        public string Address { get; set; }

        [Range(0, int.MaxValue)]
        public int CityId { get; set; }
    }
}