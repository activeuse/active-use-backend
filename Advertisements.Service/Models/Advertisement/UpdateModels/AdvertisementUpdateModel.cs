﻿namespace Advertisements.Service.Models.Advertisement.UpdateModels
{
    using System.ComponentModel.DataAnnotations;

    public class AdvertisementUpdateModel
    {
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [Required]
        public RentConfiguration RentConfiguration { get; set; }

        [Required]
        public RentItemUpdateModel RentItem { get; set; }

        public LocationUpdateModel Location { get; set; }
    }
}