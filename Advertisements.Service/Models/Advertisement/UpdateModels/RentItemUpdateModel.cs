﻿namespace Advertisements.Service.Models.Advertisement.UpdateModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities.Enums;

    public class RentItemUpdateModel
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(10000)]
        public string Details { get; set; }

        public List<FileUploadModel> Photos { get; set; }

        public ItemCategory ItemCategory { get; set; }
    }
}