﻿namespace Advertisements.Service.Models.Advertisement.CreateModels
{
    using System.ComponentModel.DataAnnotations;

    public class LocationCreateModel
    {
        public string Address { get; set; }

        [Range(0, int.MaxValue)]
        public int CityId { get; set; }
    }
}