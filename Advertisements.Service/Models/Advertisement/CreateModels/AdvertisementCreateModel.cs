﻿namespace Advertisements.Service.Models.Advertisement.CreateModels
{
    using System.ComponentModel.DataAnnotations;

    using Advertisements.Service.Models.General;

    public class AdvertisementCreateModel
    {
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }

        [Required]
        public RentItemCreateModel RentItem { get; set; }

        [Required]
        public RentConfiguration RentConfiguration { get; set; }

        [Required]
        public LocationCreateModel Location { get; set; }
    }
}