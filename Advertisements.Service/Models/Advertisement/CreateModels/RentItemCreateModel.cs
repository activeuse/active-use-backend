﻿namespace Advertisements.Service.Models.Advertisement.CreateModels
{
    using System.ComponentModel.DataAnnotations;

    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities.Enums;

    public class RentItemCreateModel
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(10000)]
        public string Details { get; set; }

        public Gender? Gender { get; set; }

        public ItemCategory ItemCategory { get; set; }

        public FileUpload[] Photos { get; set; }

        public RentItemDimensions Dimensions { get; set; }

        public RentItemPackage[] Package { get; set; }

        public RentItemProperty[] Properties { get; set; }

        public DeliveryType[] DeliveryTypes { get; set; }
    }
}