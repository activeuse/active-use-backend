﻿namespace Advertisements.Service.Models.Advertisement
{
    using System.Collections.Generic;

    using Advertisements.Service.Models.Advertisement.ViewModels;

    public class AdvertisementInfoAggregateModel
    {
        public IEnumerable<AdvertisementInfoViewModel> Advertisements { get; set; }

        public int TotalCount { get; set; }
    }
}