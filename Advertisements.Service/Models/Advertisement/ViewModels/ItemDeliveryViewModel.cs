﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using Advertisements.Service.Models.General;

    public class ItemDeliveryViewModel
    {
        public KeyValue DeliveryType { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }
    }
}