﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using System;
    using System.Collections.Generic;

    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities.Enums;

    public class AdvertisementInfoViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public ItemPhotoViewModel CoverPhoto { get; set; }

        public KeyValue Category { get; set; }

        public float FullPricePerPeriod { get; set; }

        public List<ItemDeliveryViewModel> DeliveryTypes { get; set; }

        public KeyValue RentPeriod { get; set; }

        public DateTime CreatedAt { get; set; }

        public KeyValue Status { get; set; }
    }
}