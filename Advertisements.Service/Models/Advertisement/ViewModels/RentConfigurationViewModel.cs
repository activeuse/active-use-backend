﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using Advertisements.Service.Models.General;

    public class RentConfigurationViewModel
    {
        public float FullPricePerPeriod { get; set; }

        public int? MaxRentDuration { get; set; }

        public int? MinRentDuration { get; set; }

        public KeyValue RentPeriod { get; set; }

        public KeyValue Currency { get; set; }
    }
}