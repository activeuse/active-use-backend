﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using System.Collections.Generic;

    using Advertisements.Service.Models.General;

    public class RentItemViewModel
    {
        public string Name { get; set; }

        public string Details { get; set; }

        public KeyValue Gender { get; set; }

        public KeyValue ItemCategory { get; set; }

        public List<ItemPhotoViewModel> Photos { get; set; }

        public RentItemDimensionsViewModel Dimensions { get; set; }

        public List<RentItemPackage> Package { get; set; }

        public List<RentItemProperty> Properties { get; set; }

        public List<ItemDeliveryViewModel> DeliveryTypes { get; set; }
    }
}
