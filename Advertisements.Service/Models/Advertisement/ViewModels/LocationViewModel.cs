﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    public class LocationViewModel
    {
        public string Address { get; set; }

        public int CityId { get; set; }

        public string Country { get; set; }

        public int CountryId { get; set; }

        public string City { get; set; }

        public string NativeCountryName { get; set; }
    }
}