﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using System;

    using Advertisements.Service.Models.General;

    public class AdvertisementViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public ItemPhotoViewModel CoverPhoto { get; set; }

        public RentItemViewModel RentItem { get; set; }

        public RentConfigurationViewModel RentConfiguration { get; set; }

        public KeyValue Status { get; set; }

        public LocationViewModel Location { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}