﻿namespace Advertisements.Service.Models.Advertisement.ViewModels
{
    using Advertisements.Service.Models.General;

    public class RentItemDimensionsViewModel
    {
        public float? Depth { get; set; }

        public float Height { get; set; }

        public float? Weight { get; set; }

        public float Width { get; set; }

        public KeyValue MeasureUnit { get; set; }
    }
}