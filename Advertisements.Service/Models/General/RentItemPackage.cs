﻿namespace Advertisements.Service.Models.General
{
    using System.ComponentModel.DataAnnotations;

    public class RentItemPackage
    {
        [Required]
        [MaxLength(255)]
        public string ObjectName { get; set; }

        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }
    }
}