﻿namespace Advertisements.Service.Models.General
{
    using global::Service.Core.Entities.Enums;

    public class RentItemDimensions
    {
        public float? Depth { get; set; }

        public float Height { get; set; }

        public float? Weight { get; set; }

        public float Width { get; set; }

        public MeasureUnit MeasureUnit { get; set; }
    }
}