﻿namespace Advertisements.Service.Models.General
{
    using global::Service.Core.Entities.Enums;

    public class RentConfiguration
    {
        public Currency Currency { get; set; }

        public float FullPricePerPeriod { get; set; }

        public int? MaxRentDuration { get; set; }

        public int? MinRentDuration { get; set; }

        public RentPeriod RentPeriod { get; set; }
    }
}