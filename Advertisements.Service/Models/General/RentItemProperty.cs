﻿namespace Advertisements.Service.Models.General
{
    using System.ComponentModel.DataAnnotations;

    public class RentItemProperty
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(255)]
        public string Value { get; set; }
    }
}