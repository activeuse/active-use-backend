﻿namespace Advertisements.Service.Models.General
{
    public class KeyValue
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}