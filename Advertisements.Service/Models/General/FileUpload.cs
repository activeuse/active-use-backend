﻿namespace Advertisements.Service.Models.General
{
    using System.ComponentModel.DataAnnotations;

    public sealed class FileUpload
    {
        [Required]
        public string Base64Content { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
    }
}