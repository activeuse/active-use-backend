﻿namespace Advertisements.Service
{
    using System;
    using System.Linq;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Advertisements.Service.DataAccess.Mappings;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Mappings;

    using AutoMapper;

    using global::Service.Core;

    using Payments.Service.DataAccess;

    using WebApi.StructureMap;

    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new UiMappings());
                    cfg.AddProfile(new DataAccessMappings());
                });

            var mapper = config.CreateMapper();

            GlobalConfiguration.Configuration.UseStructureMap(
                x =>
                    {
                        x.For<IMapper>().Use(mapper);
                        x.AddRegistry<CoreRegistry>();
                        x.AddRegistry<DataAccess.DataAccessRegistry>();
                        x.AddRegistry<PaymentsServiceRegistry>();
                    });
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("type", "json", new MediaTypeHeaderValue("application/json")));
        }
    }
}