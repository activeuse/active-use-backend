﻿namespace Advertisements.Service.Converters
{
    using Advertisements.Service.Models;

    using global::Service.Core.Entities;

    public static class LocationExtensions
    {
        public static CountryViewModel ToViewModel(this CountryEntity countryEntity)
        {
            return new CountryViewModel
            {
                Id = countryEntity.Id,
                Name = countryEntity.Name,
                Phone = countryEntity.Phone,
                Native = countryEntity.Native,
                Currency = countryEntity.Currency
            };
        }

        public static CityViewModel ToViewModel(this CityEntity cityEntity)
        {
            return new CityViewModel
            {
                Id = cityEntity.Id,
                Name = cityEntity.Name
            };
        }
    }
}