﻿namespace Advertisements.Service.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.Models;
    using Advertisements.Service.Models.Advertisement.CreateModels;
    using Advertisements.Service.Models.Advertisement.UpdateModels;
    using Advertisements.Service.Models.Advertisement.ViewModels;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    /// <summary>
    /// Converters
    /// </summary>
    public static class AdvertisementExtensions
    {
        /// <summary>
        /// Convert <see cref="AdvertisementCreateModel"/> to info model.
        /// </summary>
        /// <param name="createModel">The advertisement create model.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Advertisement info model.</returns>
        public static AdvertisementInfo ToInfo(this AdvertisementCreateModel createModel, string userId)
        {
            var dimensions = createModel.RentItem.Dimensions;
            var package = createModel.RentItem.Package ?? new RentItemPackage[0];
            var properties = createModel.RentItem.Properties ?? new RentItemProperty[0];
            var photos = createModel.RentItem.Images ?? new FileUpload[0];

            var model = new AdvertisementInfo
            {
                Description = createModel.Description,
                Title = createModel.Title,
                UserId = userId,
                RentItem = new RentItemInfo
                {
                    CategoryId = createModel.RentItem.ItemCategory,
                    Name = createModel.RentItem.Name,
                    Gender = createModel.RentItem.Gender,
                    Dimensions = dimensions != null ? new RentItemDimensionsInfo
                    {
                        MeasureUnit = dimensions.MeasureUnit,
                        Depth = dimensions.Depth,
                        Height = dimensions.Height,
                        Weight = dimensions.Weight,
                        Width = dimensions.Width
                    }
                    : null,
                    DeliveryTypes = createModel.RentItem.DeliveryTypes.ToList(),
                    Package = package.Select(x => new ItemPackageInfo
                    {
                        ObjectName = x.ObjectName,
                        Quantity = x.Quantity
                    }).ToList(),
                    Properties = properties.Select(x => new RentItemPropertyInfo
                    {
                        Name = x.Name,
                        Value = x.Value
                    }).ToList(),
                    Photos = photos.Select(x => new FileUploadInfo
                    {
                        Data = Convert.FromBase64String(x.Base64Content),
                        Name = x.Name
                    }).ToList()
                },
                Location = new LocationInfo
                {
                    Address = createModel.Location.Address,
                    CityId = createModel.Location.CityId
                },
                RentConfiguration = new List<RentConfigurationInfo>
                                                          {
                                                              new RentConfigurationInfo
                                                                  {
                                                                      Currency = createModel.RentConfiguration.Currency,
                                                                      FullPricePerPeriod = createModel.RentConfiguration.FullPricePerPeriod,
                                                                      MaxRentDuration = createModel.RentConfiguration.MaxRentDuration,
                                                                      MinRentDuration = createModel.RentConfiguration.MinRentDuration,
                                                                      RentPeriod = createModel.RentConfiguration.RentPeriod
                                                                  }
                                                          }
            };

            return model;
        }

        public static AdvertisementInfo ToInfo(this AdvertisementUpdateModel updateModel, string userId)
        {
            var model = new AdvertisementInfo
            {
                Id = updateModel.Id,
                Description = updateModel.Description,
                Title = updateModel.Title,
                UserId = userId,
                RentItem = new RentItemInfo
                {
                    Id = updateModel.Item.Id,
                    CategoryId = updateModel.Item.ItemCategory,
                    Name = updateModel.Item.Name,
                    Photos = updateModel.Item.Photos?.Select(x => new FileUploadInfo
                    {
                        Data = Convert.FromBase64String(x.Base64Content),
                        Name = x.Name
                    }).ToList() ?? new List<FileUploadInfo>()
                },
                Location = new LocationInfo
                {
                    Address = updateModel.Location.Address,
                    CityId = updateModel.Location.CityId
                },
                RentConfiguration = new List<RentConfigurationInfo>
                                                        {
                                                            new RentConfigurationInfo
                                                                {
                                                                    Id = updateModel.RentConfiguration.Id,
                                                                    Currency = updateModel.RentConfiguration.Currency,
                                                                    FullPricePerPeriod = updateModel.RentConfiguration.FullPricePerPeriod,
                                                                    MaxRentDuration = updateModel.RentConfiguration.MaxRentDuration,
                                                                    MinRentDuration = updateModel.RentConfiguration.MinRentDuration,
                                                                    RentPeriod = updateModel.RentConfiguration.RentPeriod
                                                                }
                                                        }
            };

            return model;
        }

        /// <summary>
        /// Converts advertisement entity to info view model.
        /// </summary>
        /// <param name="advertisement">The advertisement entity.</param>
        /// <returns>Advertisement info view model.</returns>
        public static AdvertisementInfoViewModel ToInfoViewModel(this AdvertisementEntity advertisement)
        {
            if (advertisement == null)
            {
                return null;
            }

            var coverPhoto = advertisement.RentItem.Photos.FirstOrDefault(x => x.IsThumbnail);
            var rentConfig = advertisement.RentConfiguration.First();

            return new AdvertisementInfoViewModel
            {
                Id = advertisement.Id,
                Title = advertisement.Title,
                PricePerPeriod = rentConfig.FullPricePerPeriod,
                DeliveryTypes = advertisement.RentItem.Deliveries.Select(x => (DeliveryType)x.DeliveryTypeId).ToArray(),
                RentPeriod = (RentPeriod)rentConfig.RentPeriodId,
                Category = new KeyValue
                {
                    Id = advertisement.RentItem.ItemCategory.Id,
                    Name = advertisement.RentItem.ItemCategory.Name,
                },
                Status = new KeyValue
                {
                    Id = advertisement.Status.Id,
                    Name = advertisement.Status.Name
                },
                CoverPhoto = coverPhoto != null ? new ItemPhotoViewModel
                {
                    Height = coverPhoto.Height,
                    Width = coverPhoto.Width,
                    Url = coverPhoto.CloudUrl,
                    IsThumbnail = coverPhoto.IsThumbnail,
                    Name = coverPhoto.Name
                }
                                 : null,
                CreatedAt = advertisement.CreatedAt,
            };
        }

        /// <summary>
        /// Converts advertisement entity to view model.
        /// </summary>
        /// <param name="advertisement">The advertisement entity.</param>
        /// <returns>Ad view model.</returns>
        public static AdvertisementViewModel ToViewModel(this AdvertisementEntity advertisement)
        {
            if (advertisement == null)
            {
                return null;
            }

            var rentConfig = advertisement.RentConfiguration.First();
            var dimensions = advertisement.RentItem.Dimensions;
            var package = advertisement.RentItem.Package ?? new ItemPackageEntity[0];
            var properties = advertisement.RentItem.Properties ?? new RentItemPropertyEntity[0];

            return new AdvertisementViewModel
            {
                Id = advertisement.Id,
                Title = advertisement.Title,
                Description = advertisement.Description,
                Status = new KeyValue
                {
                    Id = advertisement.Status.Id,
                    Name = advertisement.Status.Name
                },
                Item = new RentItemViewModel
                {
                    Id = advertisement.RentItem.Id,
                    Name = advertisement.RentItem.Name,
                    Gender = advertisement.RentItem.Gender?.Name,
                    Category = new KeyValue
                    {
                        Id = advertisement.RentItem.ItemCategory.Id,
                        Name = advertisement.RentItem.ItemCategory.Name,
                    },
                    Dimensions = dimensions != null ? new RentItemDimensionsViewModel
                    {
                        MeasureUnit = dimensions.MeasureUnit.Name,
                        Depth = dimensions.Depth,
                        Height = dimensions.Height,
                        Weight = dimensions.Weight,
                        Width = dimensions.Width
                    }
                    : null,
                    Package = package.Select(x => new RentItemPackage()
                    {
                        ObjectName = x.ObjectName,
                        Quantity = x.Quantity
                    }).ToList(),
                    Properties = properties.Select(x => new RentItemProperty
                    {
                        Name = x.Name,
                        Value = x.Value
                    }).ToList(),
                    Deliveries = advertisement.RentItem.Deliveries.Select(x => new ItemDeliveryViewModel
                    {
                        Description = x.DeliveryType.Description,
                        DeliveryTypeId = x.DeliveryTypeId,
                        DeliveryTypeName = x.DeliveryType.Name,
                        Price = x.DeliveryType.Price
                    }).ToList(),
                    Photos = advertisement.RentItem.Photos.Select(x => new ItemPhotoViewModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Height = x.Height,
                        Width = x.Width,
                        Url = x.CloudUrl,
                        IsThumbnail = x.IsThumbnail
                    }).ToList()
                },
                RentConfiguration = new RentConfigurationViewModel
                {
                    Id = rentConfig.Id,
                    MinRentDuration = rentConfig.MinRentDuration,
                    MaxRentDuration = rentConfig.MaxRentDuration,
                    Currency = new KeyValue
                    {
                        Id = rentConfig.Currency.Id,
                        Name = rentConfig.Currency.Name
                    },
                    FullPricePerPeriod = rentConfig.FullPricePerPeriod,
                    RentPeriod = new KeyValue
                    {
                        Id = rentConfig.RentPeriod.Id,
                        Name = rentConfig.RentPeriod.Name,
                    }
                },
                Location = new LocationViewModel
                {
                    City = advertisement.Location.City.Name,
                    Country = advertisement.Location.City.Country.Name,
                    CountryNativeName = advertisement.Location.City.Country.Native,
                    Address = advertisement.Location.Address
                },
                CreatedAt = advertisement.CreatedAt,
                UpdatedAt = advertisement.UpdatedAt
            };
        }
    }
}