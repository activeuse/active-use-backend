﻿namespace Advertisements.Service.Mappings
{
    using System;
    using System.Collections.Generic;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Models.Views;
    using Advertisements.Service.Models;
    using Advertisements.Service.Models.Advertisement.CreateModels;
    using Advertisements.Service.Models.Advertisement.ViewModels;
    using Advertisements.Service.Models.Booking;
    using Advertisements.Service.Models.General;

    using AutoMapper;

    using global::Service.Core.Entities.Enums;

    public sealed class UiMappings : Profile
    {
        public UiMappings()
        {
            /*
             * Mappings appear in the order of classes in corresponding folders
             */
            this.MapGenericModelToInfoModels();
            this.MapInfoModelsToViewModels();
            this.MapCreateModelsToInfoModels();
        }

        private void MapCreateModelsToInfoModels()
        {
            this.CreateMap<AdvertisementCreateModel, AdvertisementInfo>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CoverPhoto, opt => opt.Ignore());
            this.CreateMap<LocationCreateModel, LocationInfo>();
            this.CreateMap<RentItemCreateModel, RentItemInfo>()
                .ForMember(dest => dest.Photos, opt => opt.Ignore());
        }

        private void MapInfoModelsToViewModels()
        {
            this.CreateMap<AdvertisementStatus, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<BookingRequestStatus, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<Currency, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<DeliveryType, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<Gender, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<ItemCategory, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<MeasureUnit, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });
            this.CreateMap<RentPeriod, KeyValue>().ConstructUsing(x => new KeyValue { Id = (int)x, Name = x.ToString() });

            this.CreateMap<AdvertisementInfo, AdvertisementInfoViewModel>()
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.RentItem.ItemCategory))
                .ForMember(dest => dest.DeliveryTypes, opt => opt.MapFrom(src => src.RentItem.DeliveryTypes))
                .ForMember(dest => dest.FullPricePerPeriod, opt => opt.MapFrom(src => src.RentConfiguration.FullPricePerPeriod))
                .ForMember(dest => dest.RentPeriod, opt => opt.MapFrom(src => src.RentConfiguration.RentPeriod));

            this.CreateMap<AdvertisementInfo, AdvertisementViewModel>();
            this.CreateMap<ItemDeliveryInfoModel, ItemDeliveryViewModel>();
            this.CreateMap<LocationInfo, LocationViewModel>();
            this.CreateMap<ItemPhotoInfo, ItemPhotoViewModel>();
            this.CreateMap<RentConfigurationInfo, RentConfigurationViewModel>();
            this.CreateMap<RentItemDimensionsInfo, RentItemDimensionsViewModel>();
            this.CreateMap<RentItemInfo, RentItemViewModel>();

            //// Bookings
            this.CreateMap<IncomingBookingRequestInfo, IncomingBookingRequestViewModel>();
            this.CreateMap<OutgoingBookingRequestInfo, OutgoingBookingRequestViewModel>();
        }

        private void MapGenericModelToInfoModels()
        {
            this.CreateMap<FileUpload, FileUploadInfo>()
                .ForMember(dest => dest.Data, opt => opt.MapFrom(src => Convert.FromBase64String(src.Base64Content)));

            this.CreateMap<RentConfiguration, RentConfigurationInfo>();

            this.CreateMap<RentItemDimensions, RentItemDimensionsInfo>();
            this.CreateMap<RentItemPackage, ItemPackageInfo>();
            this.CreateMap<RentItemProperty, RentItemPropertyInfo>();

            this.CreateMap<CityInfo, CityViewModel>();
            this.CreateMap<CountryInfo, CountryViewModel>();

            this.CreateMap<DeliveryType, ItemDeliveryInfoModel>()
                .ConstructUsing(src => new ItemDeliveryInfoModel { DeliveryType = src });

            //// Bookings
  
            this.CreateMap<BookingRequest, BookingRequestInfo>()
                .ForMember(dest => dest.RequesterUserId, opt => opt.Ignore());
        }
    }
}