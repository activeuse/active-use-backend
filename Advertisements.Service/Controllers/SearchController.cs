﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.Advertisement.ViewModels;

    using AutoMapper;

    [JwtAuthentication]
    public class SearchController : BaseIdentityController
    {
        private const int DefaultPageSize = 20;

        private readonly IAdvertisementsSearchService searchService;

        private readonly IMapper mapper;

        private readonly int defaultCountOfAdvertisementsOnPage, minCountOfAdvertisementsOnPage;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchController"/> class.
        /// </summary>
        public SearchController(IAdvertisementsSearchService searchService, IMapper mapper)
        {
            this.searchService = searchService;
            this.mapper = mapper;
            this.defaultCountOfAdvertisementsOnPage = int.Parse(ConfigurationManager.AppSettings["DefaultCountOfAdvertisementsOnPage"]);
            this.minCountOfAdvertisementsOnPage = int.Parse(ConfigurationManager.AppSettings["MinCountOfAdvertisementsOnPage"]);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Search(string query, int? categoryId, int? cityId, int? page, int? take)
        {
            page = page < 0 || !page.HasValue ? 0 : page;
            take = take < this.minCountOfAdvertisementsOnPage || !take.HasValue ? this.defaultCountOfAdvertisementsOnPage : take;

            var userId = this.GetUserId();
            var advertisements = await this.searchService.Search(query, categoryId, cityId, userId, page.Value, take.Value);
            var mappedAdvertisements = this.mapper.Map<IEnumerable<AdvertisementInfoViewModel>>(advertisements);
            return this.Request.CreateResponse(mappedAdvertisements);
        }
    }
}