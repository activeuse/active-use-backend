﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class DeliveryTypesController : ApiController
    {
        private readonly IBasicRepository<DeliveryTypeEntity> deliveryTypesRepository;

        public DeliveryTypesController(IBasicRepository<DeliveryTypeEntity> deliveryTypesRepository)
        {
            this.deliveryTypesRepository = deliveryTypesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var deliveryTypes = await this.deliveryTypesRepository.GetAll();
            return deliveryTypes.Select(x => new KeyValueModel { Id = x.Id, Name = x.Name });
        }
    }
}