﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class GendersController : ApiController
    {
        private readonly IBasicRepository<GenderEntity> gendersRepository;

        public GendersController(IBasicRepository<GenderEntity> gendersRepository)
        {
            this.gendersRepository = gendersRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var genders = await this.gendersRepository.GetAll();
            return genders.Select(x => new KeyValueModel { Id = x.Id, Name = x.Name });
        }
    }
}