﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class RentPeriodsController : ApiController
    {
        private readonly IBasicRepository<RentPeriodEntity> rentPeriodsRepository;

        public RentPeriodsController(
            IBasicRepository<RentPeriodEntity> rentPeriodsRepository)
        {
            this.rentPeriodsRepository = rentPeriodsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var rentPeriods = await this.rentPeriodsRepository.GetAll();
            return rentPeriods.Select(x => new KeyValueModel { Id = (int)x.Id, Name = x.Name });
        }
    }
}