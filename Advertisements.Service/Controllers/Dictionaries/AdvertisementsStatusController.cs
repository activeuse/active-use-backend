﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class AdvertisementsStatusController : ApiController
    {
        private readonly IBasicRepository<AdvertisementStatusEntity> advertisementsStatusRepository;

        public AdvertisementsStatusController(IBasicRepository<AdvertisementStatusEntity> advertisementsStatusRepository)
        {
            this.advertisementsStatusRepository = advertisementsStatusRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var categories = await this.advertisementsStatusRepository.GetAll();
            return categories.Select(x => new KeyValueModel { Id = (int)x.Id, Name = x.Name });
        }
    }
}