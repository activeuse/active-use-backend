﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class CurrenciesController : ApiController
    {
        private readonly IBasicRepository<CurrencyEntity> currenciesRepository;

        public CurrenciesController(IBasicRepository<CurrencyEntity> currenciesRepository)
        {
            this.currenciesRepository = currenciesRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var currencies = await this.currenciesRepository.GetAll();
            return currencies.Select(x => new KeyValueModel { Id = (int)x.Id, Name = x.Name });
        }
    }
}