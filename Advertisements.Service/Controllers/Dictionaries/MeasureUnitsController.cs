﻿namespace Advertisements.Service.Controllers.Dictionaries
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.General;

    using global::Service.Core.Entities;

    [JwtAuthentication]
    public class MeasureUnitsController : ApiController
    {
        private readonly IBasicRepository<MeasureUnitEntity> measureUnitsRepository;

        public MeasureUnitsController(IBasicRepository<MeasureUnitEntity> measureUnitsRepository)
        {
            this.measureUnitsRepository = measureUnitsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<KeyValueModel>> Get()
        {
            var measureUnits = await this.measureUnitsRepository.GetAll();
            return measureUnits.Select(x => new KeyValueModel { Id = x.Id, Name = x.Name });
        }
    }
}