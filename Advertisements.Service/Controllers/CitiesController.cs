﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models;

    using AutoMapper;

    using global::Service.Core.Caching;

    /// <summary>
    /// Provides cities data.
    /// </summary>
    [JwtAuthentication]
    public class CitiesController : ApiController
    {
        private readonly ILocationsService locationsService;

        private readonly IMapper mapper;

        private readonly ISimpleCache simpleCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="CitiesController"/> class.
        /// </summary>
        public CitiesController(ILocationsService locationsService, IMapper mapper, ISimpleCache simpleCache)
        {
            this.locationsService = locationsService;
            this.mapper = mapper;
            this.simpleCache = simpleCache;
        }

        /// <summary>
        /// Gets cities of specified country.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <returns>List of cities.</returns>
        public async Task<HttpResponseMessage> Get(int countryId)
        {
            var cachedCities = this.simpleCache.Get($"cities_countryId={countryId}");
            if (cachedCities != null)
            {
                return this.Request.CreateResponse(cachedCities);
            }

            var cities = await this.locationsService.GetCities(countryId);
            var mappedCities = this.mapper.Map<IEnumerable<CityViewModel>>(cities);
            this.simpleCache.Add($"cities_countryId={countryId}", mappedCities);
            return this.Request.CreateResponse(mappedCities);
        }
    }
}