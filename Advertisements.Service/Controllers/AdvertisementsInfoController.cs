﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.Advertisement;
    using Advertisements.Service.Models.Advertisement.ViewModels;

    using AutoMapper;

    /// <summary>
    /// Controller for advertisements info management. Info is a short description with reduced number of properties.
    /// Info models are used where reduced set of properties is required.
    /// /// </summary>
    [JwtAuthentication]
    public sealed class AdvertisementsInfoController : BaseIdentityController
    {
        private readonly IAdvertisementsService advertisementsService;

        private readonly IMapper mapper;

        private readonly int defaultCountOfAdvertisementsOnPage, minCountOfAdvertisementsOnPage;

        /// <inheritdoc />
        public AdvertisementsInfoController(IAdvertisementsService advertisementsService, IMapper mapper)
        {
            this.advertisementsService = advertisementsService;
            this.mapper = mapper;
            this.defaultCountOfAdvertisementsOnPage = int.Parse(ConfigurationManager.AppSettings["DefaultCountOfAdvertisementsOnPage"]);
            this.minCountOfAdvertisementsOnPage = int.Parse(ConfigurationManager.AppSettings["MinCountOfAdvertisementsOnPage"]);
        }

        /// <summary>
        /// Gets basic information about user's ads.
        /// </summary>
        /// <returns>Basic info about user's ads.</returns>
        [HttpGet]
        public async Task<AdvertisementInfoAggregateModel> GetInfo(int? page, int? take)
        {
            page = page < 0 || !page.HasValue ? 0 : page;
            take = take < this.minCountOfAdvertisementsOnPage || !take.HasValue ? this.defaultCountOfAdvertisementsOnPage : take;

            var userId = this.GetUserId();
            var userAdvertisements = await this.advertisementsService.Get(page.Value, take.Value, userId);
            var adsCount = await this.advertisementsService.GetAdvertisementsCount(userId);

            var mappedAdvertisements = this.mapper.Map<IEnumerable<AdvertisementInfoViewModel>>(userAdvertisements);
            var aggregateModel =
                new AdvertisementInfoAggregateModel
                    {
                        Advertisements = mappedAdvertisements,
                        TotalCount = adsCount
                    };

            return aggregateModel;
        }
    }
}