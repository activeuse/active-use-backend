﻿namespace Advertisements.Service.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.UI;

    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.Filters;

    using global::Service.Core.Caching;
    using global::Service.Core.Entities;

    /// <summary>
    /// Returns properties used when creating ad.
    /// </summary>
    [JwtAuthentication]
    public class AdvertisementPropertiesController : ApiController
    {
        private readonly IBasicRepository<AdvertisementStatusEntity> advertisementsStatusRepository;

        private readonly IBasicRepository<ItemCategoryEntity> categoriesRepository;

        private readonly IBasicRepository<CurrencyEntity> currenciesRepository;

        private readonly IBasicRepository<DeliveryTypeEntity> deliveryTypesRepository;

        private readonly IBasicRepository<GenderEntity> gendersRepository;

        private readonly IBasicRepository<MeasureUnitEntity> measureUnitsRepository;

        private readonly IBasicRepository<RentPeriodEntity> rentPeriodsRepository;

        private readonly ISimpleCache simpleCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvertisementPropertiesController" /> class.
        /// </summary>
        public AdvertisementPropertiesController(
            IBasicRepository<RentPeriodEntity> rentPeriodsRepository,
            IBasicRepository<MeasureUnitEntity> measureUnitsRepository,
            IBasicRepository<ItemCategoryEntity> categoriesRepository,
            IBasicRepository<GenderEntity> gendersRepository,
            IBasicRepository<CurrencyEntity> currenciesRepository,
            IBasicRepository<DeliveryTypeEntity> deliveryTypesRepository,
            IBasicRepository<AdvertisementStatusEntity> advertisementsStatusRepository,
            ISimpleCache simpleCache)
        {
            this.rentPeriodsRepository = rentPeriodsRepository;
            this.measureUnitsRepository = measureUnitsRepository;
            this.categoriesRepository = categoriesRepository;
            this.gendersRepository = gendersRepository;
            this.currenciesRepository = currenciesRepository;
            this.deliveryTypesRepository = deliveryTypesRepository;
            this.advertisementsStatusRepository = advertisementsStatusRepository;
            this.simpleCache = simpleCache;
        }

        /// <summary>
        /// Gets ad's properties.
        /// </summary>
        /// <returns>Ad's properties.</returns>
        [System.Web.Http.HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var cachedItems = this.simpleCache.Get("AdvertisementProperties");
            if (cachedItems != null)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, cachedItems);
            }

            var rentPeriods = await this.rentPeriodsRepository.GetAll();
            var measureUnits = await this.measureUnitsRepository.GetAll();
            var categories = await this.categoriesRepository.GetAll();
            var genders = await this.gendersRepository.GetAll();
            var currencies = await this.currenciesRepository.GetAll();
            var deliveryTypes = await this.deliveryTypesRepository.GetAll();
            var advertisementStatus = await this.advertisementsStatusRepository.GetAll();

            var dataObject = new
            {
                rentPeriods,
                measureUnits,
                categories,
                genders,
                currencies,
                deliveryTypes,
                advertisementStatus
            };

            this.simpleCache.Add("AdvertisementProperties", dataObject);

            return this.Request.CreateResponse(HttpStatusCode.OK, dataObject);
        }
    }
}