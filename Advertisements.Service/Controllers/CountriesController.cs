﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models;

    using AutoMapper;

    using global::Service.Core.Caching;

    /// <summary>
    /// Provides countries data.
    /// </summary>
    [JwtAuthentication]
    public class CountriesController : ApiController
    {
        private readonly ILocationsService locationsService;

        private readonly IMapper mapper;

        private readonly ISimpleCache simpleCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesController"/> class.
        /// </summary>
        public CountriesController(ILocationsService locationsService, IMapper mapper, ISimpleCache simpleCache)
        {
            this.locationsService = locationsService;
            this.mapper = mapper;
            this.simpleCache = simpleCache;
        }

        /// <summary>
        /// Gets list of countries.
        /// </summary>
        /// <returns>List of countries.</returns>
        public async Task<HttpResponseMessage> Get()
        {
            var cachedCountries = this.simpleCache.Get("countries");
            if (cachedCountries != null)
            {
                return this.Request.CreateResponse(cachedCountries);
            }

            var countries = await this.locationsService.GetCountries();
            var mappedCountries = this.mapper.Map<IEnumerable<CountryViewModel>>(countries);
            this.simpleCache.Add("countries", mappedCountries);

            return this.Request.CreateResponse(mappedCountries);
        }
    }
}