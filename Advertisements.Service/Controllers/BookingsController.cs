﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.Booking;

    using AutoMapper;

    using global::Service.Core.Entities.Enums;
    using global::Service.Core.Utils;

    [JwtAuthentication]
    [RoutePrefix("api/bookings")]
    public class BookingsController : BaseIdentityController
    {
        private readonly IBookingService bookingService;

        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingsController"/> class.
        /// </summary>
        public BookingsController(IBookingService bookingService, IMapper mapper)
        {
            this.bookingService = bookingService;
            this.mapper = mapper;
        }

        [HttpPost]
        [ValidateModelState]
        public async Task<HttpResponseMessage> MakeRequest(BookingRequest request)
        {
            Guard.ArgumentNotNull(request, nameof(request));

            if (request.StartDate >= request.EndDate)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "End date cant precede start date");
            }

            if (request.EndDate.Subtract(request.StartDate).Days < 1)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, "You can bool and item for at least one day");
            }

            var userId = this.GetUserId();
            var mappedRequest = this.mapper.Map<BookingRequestInfo>(request);
            mappedRequest.RequesterUserId = userId;

            var result = await this.bookingService.Request(mappedRequest, request.SecretPhrase);

            switch (result)
            {
                case BookingRequestResult.AlreadyExists:
                    return this.Request.CreateErrorResponse(HttpStatusCode.Conflict, "You already have requested this item");
                case BookingRequestResult.DatesNotAvailable:
                    return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Item is not available for provided range of dates");
                case BookingRequestResult.MaxNumberOfActiveRequestsExceeded:
                    return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "You have exceeded the number of bookings");
                case BookingRequestResult.CantBookOwnItem:
                    return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Can't book own item");
                case BookingRequestResult.InsufficientBalance:
                    return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Insufficient balance");
                default:
                    return this.Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [Route("incoming")]
        [HttpGet]
        public async Task<IEnumerable<IncomingBookingRequestViewModel>> Incoming()
        {
            var bookings = await this.bookingService.GetIncomingRequests(this.GetUserId());
            var mappedBookings = this.mapper.Map<IEnumerable<IncomingBookingRequestViewModel>>(bookings);
            return mappedBookings;
        }

        [Route("incoming/{requestId:int}/approve")]
        [HttpPost]
        public async Task Approve([FromUri]int requestId)
        {
            await this.bookingService.ApproveRequest(requestId, this.GetUserId());
        }

        [Route("incoming/{requestId:int}/reject")]
        [HttpPost]
        public async Task Reject([FromUri]int requestId)
        {
            await this.bookingService.RejectRequest(requestId, this.GetUserId());
        }

        [Route("outgoing")]
        [HttpGet]
        public async Task<IEnumerable<OutgoingBookingRequestViewModel>> Outgoing()
        {
            var bookings = await this.bookingService.GetOutgoingRequests(this.GetUserId());
            var mappedBookings = this.mapper.Map<IEnumerable<OutgoingBookingRequestViewModel>>(bookings);
            return mappedBookings;
        }

        [Route("outgoing/{requestId:int}")]
        [HttpDelete]
        public async Task DeleteOutgoing([FromUri]int requestId)
        {
            await this.bookingService.DeleteOutgoingRequest(requestId, this.GetUserId());
        }
    }
}