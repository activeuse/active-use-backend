﻿namespace Advertisements.Service.Controllers
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Services;
    using Advertisements.Service.Filters;
    using Advertisements.Service.Models.Advertisement.CreateModels;
    using Advertisements.Service.Models.Advertisement.ViewModels;

    using AutoMapper;

    using global::Service.Core.Caching;
    using global::Service.Core.Entities.Enums;
    using global::Service.Core.Utils;

    /// <summary>
    /// Advertisement API
    /// </summary>
    [JwtAuthentication]
    public sealed class AdvertisementsController : BaseIdentityController
    {
        private readonly IAdvertisementsService advertisementsService;

        private readonly IMapper mapper;

        private readonly ISimpleCache cache;

        private readonly int maxPhotosInAd, maxPhotoSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvertisementsController"/> class.
        /// </summary>
        public AdvertisementsController(IAdvertisementsService advertisementsService, IMapper mapper, ISimpleCache cache)
        {
            this.advertisementsService = advertisementsService;
            this.mapper = mapper;
            this.cache = cache;

            this.maxPhotosInAd = int.Parse(ConfigurationManager.AppSettings["MaxPhotosInAd"]);
            this.maxPhotoSize = int.Parse(ConfigurationManager.AppSettings["MaxUploadItemPhotoSizeMegabytes"]);
        }

        /// <summary>
        /// Creates the advertisement.
        /// </summary>
        /// <param name="createModel">The advertisement create model.</param>
        /// <returns>ID of created advertisement.</returns>
        [HttpPost]
        [ValidateModelState]
        public async Task<HttpResponseMessage> Create(AdvertisementCreateModel createModel)
        {
            var userId = this.GetUserId();
            var infoModel = this.mapper.Map<AdvertisementInfo>(createModel);
            var photos = this.mapper.Map<List<FileUploadInfo>>(createModel.RentItem.Photos);

            var validationResult = this.RunPhotoValidation(photos);
            if (validationResult != null)
            {
                return validationResult;
            }

            var advertisementId = await this.advertisementsService.Create(infoModel, photos, userId);
            return this.Request.CreateResponse(HttpStatusCode.OK, advertisementId);
        }

        /// <summary>
        /// Gets advertisement by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier of advertisement.</param>
        /// <returns>Advertisement view model.</returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            var userId = this.GetUserId();
            var adKey = this.GetAdvertisementKey(id);

            var cachedItem = this.cache.Get(adKey);
            if (cachedItem != null)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, cachedItem);
            }

            var advertisement = await this.advertisementsService.Get(id, userId);
            if (advertisement == null)
            {
                return this.Request.CreateErrorResponse(
                    HttpStatusCode.NotFound,
                    "Advertisement not found.");
            }

            var mappedAdvertisement = this.mapper.Map<AdvertisementViewModel>(advertisement);
            this.cache.Add(adKey, mappedAdvertisement);

            return this.Request.CreateResponse(HttpStatusCode.OK, mappedAdvertisement);

        }

        /// <summary>
        /// Deletes advertisement with specified identifier.
        /// </summary>
        /// <param name="id">The advertisement's identifier.</param>
        /// <returns><see cref="Task"/></returns>
        [HttpDelete]
        public async Task Delete(int id)
        {
            var userId = this.GetUserId();
            var adKey = this.GetAdvertisementKey(id);

            await this.advertisementsService.Delete(id, userId);

            this.cache.Invalidate(adKey);
        }

        ///// <summary>
        ///// Updates advertisement.
        ///// </summary>
        ///// <param name="id">The advertisement's identifier.</param>
        ///// <param name="model">The ad update model.</param>
        ///// <returns><see cref="Task"/></returns>
        //[HttpPut]
        //[ValidateModelState]
        //public async Task<HttpResponseMessage> Update(int id, [FromBody]AdvertisementUpdateModel model)
        //{
        //    var infoModel = model.ToInfo(this.GetUserId());

        //    var validationResult = this.RunPhotoValidation(infoModel.RentItem.Photos);
        //    if (validationResult != null)
        //    {
        //        return validationResult;
        //    }

        //    await this.advertisementsService.Update(infoModel);

        //    return this.Request.CreateResponse(HttpStatusCode.OK);
        //}

        /// <summary>
        /// Performs complex photo validation.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <returns>Response message.</returns>
        private HttpResponseMessage RunPhotoValidation(ICollection<FileUploadInfo> files)
        {
            if (files.Count() > this.maxPhotosInAd)
            {
                return this.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest,
                    $"Maximum {this.maxPhotosInAd} images are allowed per advertisement.");
            }

            if (!this.ValidatePhotoExtensions(files))
            {
                return this.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest,
                    "File of unsupported type was uploaded.");
            }

            var photoSizesValid = this.ValidatePhotoSize(files);

            if (!photoSizesValid)
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"Too big photos. Maximum image size is {this.maxPhotoSize} Mb.");
            }

            return null;
        }

        private string GetAdvertisementKey(int id)
        {
            return $"Adv_{this.GetUserId()}_{id}";
        }

        /// <summary>
        /// Validates photo extensions.
        /// </summary>
        /// <param name="photos">The photos.</param>
        /// <returns><c>true</c> if all photos have valid extension, <c>false</c> otherwise.</returns>
        private bool ValidatePhotoExtensions(IEnumerable<FileUploadInfo> photos)
        {
            return photos.All(x => ImageExtensionValidator.FindExtension(x.Data.Take(5).ToArray()) != ImageExtension.Unknown);
        }

        /// <summary>
        /// Validates size of the photo.
        /// </summary>
        /// <param name="photos">The photos.</param>
        /// <returns><c>true</c> if photo is valid, otherwise <c>false</c></returns>
        private bool ValidatePhotoSize(IEnumerable<FileUploadInfo> photos)
        {
            return photos.All(x => x.Data.Length <= this.maxPhotoSize * 1024 * 1024);
        }
    }
}