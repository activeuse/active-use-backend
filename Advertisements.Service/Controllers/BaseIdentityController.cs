﻿namespace Advertisements.Service.Controllers
{
    using System.Diagnostics;
    using System.Web.Http;

    using Advertisements.Service.Filters;

    using Microsoft.AspNet.Identity;

    /// <summary>
    /// Base controller for operations which require authorization.
    /// </summary>
    public class BaseIdentityController : ApiController
    {
        /// <summary>
        /// Gets ID of authorized user.
        /// </summary>
        /// <returns>User's ID.</returns>
        [DebuggerStepThrough]
        protected string GetUserId()
        {
            return this.User.Identity.GetUserId();
        }
    }
}