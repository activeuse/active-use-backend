namespace Payments.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EvtTransRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EvtTransactionEntities", newName: "EvtTransactions");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.EvtTransactions", newName: "EvtTransactionEntities");
        }
    }
}
