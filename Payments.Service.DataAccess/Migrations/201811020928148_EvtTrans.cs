namespace Payments.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EvtTrans : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EvtTransactionEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        From = c.String(),
                        To = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Message = c.String(),
                        UserId = c.String(),
                        BookingRequestId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EvtTransactionEntities");
        }
    }
}
