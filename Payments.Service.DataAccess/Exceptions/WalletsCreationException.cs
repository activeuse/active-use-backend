﻿namespace Payments.Service.DataAccess.Exceptions
{
    using System;

    public sealed class WalletsCreationException : Exception
    {
        public WalletsCreationException()
        {
        }

        public WalletsCreationException(string message)
            : base(message)
        {
        }

        public WalletsCreationException(Exception ex, string message)
            : base(message, ex)
        {
        }
    }
}