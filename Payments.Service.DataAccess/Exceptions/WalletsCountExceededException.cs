﻿namespace Payments.Service.DataAccess.Exceptions
{
    using System;

    public sealed class WalletsCountExceededException : Exception
    {
        public WalletsCountExceededException()
        {
        }

        public WalletsCountExceededException(string message)
            : base(message)
        {
        }

        public WalletsCountExceededException(Exception ex, string message)
            : base(message, ex)
        {
        }
    }
}