﻿namespace Payments.Service.DataAccess.Models
{
    using Newtonsoft.Json;

    public sealed class EvtTransactionModel
    {
        [JsonProperty("transsum")]
        public string Amount { get; set; }

        [JsonProperty("pubkey1")]
        public string From { get; set; }

        [JsonProperty("keyphrase")]
        public string KeyPhrase { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("pubkey2")]
        public string To { get; set; }
    }
}