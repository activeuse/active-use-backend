﻿namespace Payments.Service.DataAccess.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using global::Service.Core.Entities;

    [Table("EvtTransactions")]
    public class EvtTransactionEntity : IDatedEntity
    {
        public decimal Amount { get; set; }

        public int BookingRequestId { get; set; }

        /// <inheritdoc />
        public DateTime CreatedAt { get; set; }

        public string From { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Message { get; set; }

        public string To { get; set; }

        /// <inheritdoc />
        public DateTime UpdatedAt { get; set; }

        public string UserId { get; set; }
    }
}