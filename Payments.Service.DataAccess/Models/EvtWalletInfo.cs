﻿namespace Payments.Service.DataAccess.Models
{
    using System;

    public class EvtWalletInfo
    {
        public string Address { get; set; }

        public decimal Balance { get; set; }

        public DateTime CreatedAt { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }
    }
}