﻿namespace Payments.Service.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    using global::Service.Core.Entities.Payments;

    using Payments.Service.DataAccess.Context;
    using Payments.Service.DataAccess.Models;

    internal sealed class EvtWalletsRepository : IEvtWalletsRepository
    {
        public async Task<EvtWalletEntity> GetWallet(string walletAddress, string userId)
        {
            using (var context = new Context())
            {
                return await context.EvtWallets.FirstOrDefaultAsync(x => x.Address == walletAddress && x.UserId == userId);
            }
        }

        public async Task SaveTransaction(EvtTransactionEntity transaction)
        {
            using (var context = new Context())
            {
                context.EvtTransactions.Add(transaction);
                await context.SaveChangesAsync();
            }
        }

        public async Task Activate(string userId, int walletId)
        {
            using (var context = new Context())
            {
                var allWallets = context.EvtWallets.Where(x => x.UserId == userId);
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var wallet in allWallets)
                        {
                            wallet.IsActive = wallet.Id == walletId;
                        }

                        await context.SaveChangesAsync();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public async Task<int> Create(string userId, string address)
        {
            using (var context = new Context())
            {
                var wallet = new EvtWalletEntity
                {
                    UserId = userId.ToLower(),
                    Address = address,
                    IsActive = false,
                };

                context.EvtWallets.Add(wallet);

                await context.SaveChangesAsync();

                return wallet.Id;
            }
        }

        public async Task Deactivate(string userId, int walletId)
        {
            using (var context = new Context())
            {
                var wallet = await context.EvtWallets.FirstOrDefaultAsync(x => x.UserId == userId && x.Id == walletId);
                if (wallet == null)
                {
                    return;
                }

                wallet.IsActive = false;

                await context.SaveChangesAsync();
            }
        }

        public async Task<int> GetCount(string userId)
        {
            using (var context = new Context())
            {
                return await context.EvtWallets.CountAsync(x => x.UserId == userId);
            }
        }

        public async Task<int?> WalletExists(string userId, string address)
        {
            using (var context = new Context())
            {
                var wallet = await context.EvtWallets.FirstOrDefaultAsync(x => x.UserId == userId && x.Address == address);
                return wallet?.Id;
            }
        }

        public async Task<EvtWalletEntity> GetActiveWallet(string userId, string address)
        {
            using (var context = new Context())
            {
                var wallet = await context.EvtWallets.FirstOrDefaultAsync(x => x.UserId == userId && x.Address == address && x.IsActive);
                return wallet;
            }
        }

        public async Task<List<EvtWalletEntity>> GetAll(string userId)
        {
            using (var context = new Context())
            {
                return await context.EvtWallets.Where(x => x.UserId == userId)
                           .OrderByDescending(x => x.CreatedAt)
                           .ToListAsync();
            }
        }
    }
}