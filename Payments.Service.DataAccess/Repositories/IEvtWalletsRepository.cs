﻿namespace Payments.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::Service.Core.Entities.Payments;

    using Payments.Service.DataAccess.Models;

    internal interface IEvtWalletsRepository
    {
        Task<EvtWalletEntity> GetWallet(string walletAddress, string userId);

        Task SaveTransaction(EvtTransactionEntity transaction);

        Task Activate(string userId, int walletId);

        Task<int> Create(string userId, string address);

        Task Deactivate(string userId, int walletId);

        Task<int> GetCount(string userId);

        Task<List<EvtWalletEntity>> GetAll(string userId);

        Task<int?> WalletExists(string userId, string address);

        Task<EvtWalletEntity> GetActiveWallet(string userId, string address);
    }
}