﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payments.Service.DataAccess
{
    using Payments.Service.DataAccess.Repositories;
    using Payments.Service.DataAccess.Services;

    using StructureMap;

    public sealed class PaymentsServiceRegistry : Registry
    {
        public PaymentsServiceRegistry()
        {
            this.RegisterRepositories();
            this.RegisterServices();
        }

        private void RegisterRepositories()
        {
            this.For<IEvtWalletsRepository>().Use<EvtWalletsRepository>();
        }

        private void RegisterServices()
        {
            this.For<IEvtWalletsService>().Use<EvtWalletsService>();
        }
    }
}
