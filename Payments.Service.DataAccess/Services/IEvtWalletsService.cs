﻿namespace Payments.Service.DataAccess.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::Service.Core.Entities.Payments;

    using Payments.Service.DataAccess.Models;

    public interface IEvtWalletsService
    {
        Task<int> CreateWallet(string userId, string secretPhrase);

        Task Deactivate(int walletId, string userId);

        Task Activate(int walletId, string userId);

        Task<List<EvtWalletInfo>> GetAllWallets(string userId);

        Task<EvtWalletInfo> GetActiveWallet(string secretPhrase, string userId);

        Task Transfer(
            decimal amount,
            int bookingRequestId,
            string to,
            string secretPhrase,
            string message,
            string userId);
    }
}