﻿namespace Payments.Service.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using global::Service.Core.Entities.Payments;
    using global::Service.Core.Exceptions;
    using global::Service.Core.Logging;
    using global::Service.Core.Sql;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Payments.Service.DataAccess.Exceptions;
    using Payments.Service.DataAccess.Models;
    using Payments.Service.DataAccess.Repositories;

    internal sealed class EvtWalletsService : IEvtWalletsService
    {
        private readonly Uri evtNetworkEndpoint;

        private readonly IEvtWalletsRepository evtWalletsRepository;

        private readonly IDatabaseLogger databaseLogger;

        private readonly int maxEvtWalletsCount;

        public EvtWalletsService(IEvtWalletsRepository evtWalletsRepository, IDatabaseLogger databaseLogger)
        {
            this.evtWalletsRepository = evtWalletsRepository;
            this.databaseLogger = databaseLogger;
            this.maxEvtWalletsCount = int.Parse(ConfigurationManager.AppSettings["MaxEvtWalletsCount"]);
            this.evtNetworkEndpoint = new Uri(ConfigurationManager.AppSettings["EvtNetworkEndpoint"]);
        }

        public async Task Transfer(decimal amount, int bookingRequestId, string to, string secretPhrase, string message, string userId)
        {
            var url = this.BuildCreateWalletUri(secretPhrase);

            var walletAddress = await this.CreateWalletInternal(url);
            var userWallet = await this.evtWalletsRepository.GetWallet(walletAddress, userId);
            if (userWallet == null)
            {
                throw new ItemNotFoundException("Wallet not found");
            }

            if (!userWallet.IsActive)
            {
                throw new EntityBadStateException("Wallet must be active");
            }

            var transaction = new EvtTransactionModel
            {
                From = walletAddress,
                To = to,
                Amount = $"{amount:0.00000} S#1",
                KeyPhrase = secretPhrase,
                Message = message
            };

            var transferUrl = this.BuildTransactionUri();
            await this.Transfer(transferUrl, transaction);

            await this.evtWalletsRepository.SaveTransaction(
                new EvtTransactionEntity
                {
                    Amount = amount,
                    BookingRequestId = bookingRequestId,
                    From = walletAddress,
                    Message = message,
                    To = to,
                    UserId = userId
                });
        }

        public async Task Activate(int walletId, string userId)
        {
            await this.evtWalletsRepository.Activate(userId, walletId);
        }

        public async Task<EvtWalletInfo> GetActiveWallet(string secretPhrase, string userId)
        {
            var url = this.BuildCreateWalletUri(secretPhrase);

            var walletAddress = await this.CreateWalletInternal(url);

            if (string.IsNullOrEmpty(walletAddress))
            {
                throw new WalletsCreationException();
            }

            var wallet = await this.evtWalletsRepository.GetActiveWallet(userId, walletAddress);
            return wallet != null ? new EvtWalletInfo
            {
                Id = wallet.Id,
                Address = wallet.Address,
                CreatedAt = wallet.CreatedAt,
                IsActive = wallet.IsActive,
                Balance = await this.GetBalance(this.BuildBalanceUri(walletAddress))
            } : null;
        }

        public async Task<int> CreateWallet(string userId, string secretPhrase)
        {
            var walletsCount = await this.evtWalletsRepository.GetCount(userId);
            if (walletsCount >= this.maxEvtWalletsCount)
            {
                throw new WalletsCountExceededException();
            }

            var url = this.BuildCreateWalletUri(secretPhrase);

            var walletAddress = await this.CreateWalletInternal(url);

            if (string.IsNullOrEmpty(walletAddress))
            {
                throw new WalletsCreationException();
            }

            await this.databaseLogger.LogEvent(
                LogType.Create,
                new { Object = "EVT Wallet", UserId = userId, Address = walletAddress });

            var walletId = await this.evtWalletsRepository.WalletExists(userId, walletAddress);

            if (!walletId.HasValue)
            {
                walletId = await this.evtWalletsRepository.Create(userId.ToLower(), walletAddress);
            }

            await this.SetUserEvtWalletExists(userId);

            return walletId.Value;
        }

        public async Task Deactivate(int walletId, string userId)
        {
            await this.evtWalletsRepository.Deactivate(userId, walletId);
        }

        public async Task<List<EvtWalletInfo>> GetAllWallets(string userId)
        {
            var wallets = await this.evtWalletsRepository.GetAll(userId);
            var walletInfos = wallets.Select(
                x => new EvtWalletInfo
                {
                    Id = x.Id,
                    Address = x.Address,
                    CreatedAt = x.CreatedAt,
                    IsActive = x.IsActive
                }).ToList();

            foreach (var wallet in walletInfos)
            {
                var balance = await this.GetBalance(this.BuildBalanceUri(wallet.Address));
                wallet.Balance = balance;
            }

            return walletInfos;
        }

        private async Task SetUserEvtWalletExists(string userId)
        {
            const string Query = "update [AspNetUsers] set [EvtWalletExists] = 1 where [Id] = @userId;";

            using (var connection = await SqlFactory.CreateConnection(ConfigurationManager.ConnectionStrings["Users"].ConnectionString))
            {
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("userId", userId);

                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        private Uri BuildCreateWalletUri(string secretPhrase)
        {
            return new Uri(this.evtNetworkEndpoint, new Uri($"name?passphrase={secretPhrase}", UriKind.Relative));
        }

        private Uri BuildBalanceUri(string address)
        {
            return new Uri(this.evtNetworkEndpoint, new Uri($"{address}/balance", UriKind.Relative));
        }

        private Uri BuildTransactionUri()
        {
            return new Uri(this.evtNetworkEndpoint, new Uri("sendto", UriKind.Relative));
        }

        private async Task<string> CreateWalletInternal(Uri url)
        {
            string walletAddress;
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetStringAsync(url);
                    walletAddress = (string)JToken.Parse(response)["name"];
                }
                catch (Exception ex)
                {
                    await this.databaseLogger.LogEvent(LogType.Error, new { Exception = ex.Message, StackTrace = ex.StackTrace, Method = "CreateWalletInternal" });
                    throw;
                }
            }

            return walletAddress;
        }

        private async Task<decimal> GetBalance(Uri balanceEndpoint)
        {
            decimal balance = 0;
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetStringAsync(balanceEndpoint);
                    var tokensArray = JToken.Parse(response)["tokens"];
                    await this.databaseLogger.LogEvent(
                        LogType.Info,
                        new { Mathod = "GetBalance", Url = balanceEndpoint.ToString(), Response = response });

                    var b = tokensArray.Select(x => x.ToString()).FirstOrDefault(x => x.EndsWith("S#1"))?.Split(' ').First();

                    balance = decimal.Parse(b ?? "0", NumberStyles.Any, CultureInfo.CurrentCulture);
                }
                catch (Exception ex)
                {
                    await this.databaseLogger.LogEvent(LogType.Error, new { Exception = ex.Message, StackTrace = ex.StackTrace, Method = "GetBalance" });
                    // throw;
                }
            }

            return balance;
        }

        private async Task Transfer(Uri transferUrl, EvtTransactionModel transaction)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var content = new StringContent(
                        JsonConvert.SerializeObject(transaction),
                        Encoding.UTF8,
                        "application/json");

                    await client.PostAsync(transferUrl, content);
                }
                catch (Exception ex)
                {
                    await this.databaseLogger.LogEvent(LogType.Error, new { Exception = ex.Message, StackTrace = ex.StackTrace, Method = "GetBalance" });
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}