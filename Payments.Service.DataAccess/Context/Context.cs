﻿namespace Payments.Service.DataAccess.Context
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Payments;
    using global::Service.Core.Logging;

    using Payments.Service.DataAccess.Models;

    internal sealed class Context : DbContext
    {
        public Context()
            : base("Payments")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<Context>());
            // ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 600;

            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            objectContext.SavingChanges += (sender, args) =>
                {
                    var now = DateTime.UtcNow;
                    foreach (var entry in this.ChangeTracker.Entries<IDatedEntity>())
                    {
                        var entity = entry.Entity;
                        switch (entry.State)
                        {
                            case EntityState.Added:
                                entity.CreatedAt = now;
                                entity.UpdatedAt = now;
                                break;
                            case EntityState.Modified:
                                entity.UpdatedAt = now;
                                break;
                        }
                    }

                    this.ChangeTracker.DetectChanges();
                };
        }

        public DbSet<EvtWalletEntity> EvtWallets { get; set; }

        public DbSet<EvtTransactionEntity> EvtTransactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}