﻿namespace Advertisements.Service.DataAccess.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Advertisements.Service.DataAccess.Models;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    public static class Converters
    {
        public static AdvertisementEntity ToEntity(this AdvertisementInfo info, List<ItemPhotoEntity> photos)
        {
            var rentConfig = info.RentConfiguration.First();
            var packages = info.RentItem.Package ?? new List<ItemPackageInfo>();
            var properties = info.RentItem.Properties ?? new List<RentItemPropertyInfo>();
            var dims = info.RentItem.Dimensions;

            photos.ForEach(x => x.RentItemId = info.Id);

            return new AdvertisementEntity
            {
                Id = info.Id,
                UserId = info.UserId,
                Title = info.Title,
                Description = info.Description,
                StatusId = (int)AdvertisementStatus.New,
                RentItem = new RentItemEntity
                {
                    Id = info.RentItem.Id,
                    Photos = photos,
                    Name = info.RentItem.Name,
                    ItemCategoryId = (int)info.RentItem.CategoryId,
                    GenderId = (int?)info.RentItem.Gender,
                    Package = packages.Select(x => new ItemPackageEntity
                    {
                        ObjectName = x.ObjectName,
                        Quantity = x.Quantity
                    }).ToList(),
                    Properties = properties.Select(x => new RentItemPropertyEntity
                    {
                        Name = x.Name,
                        Value = x.Value
                    }).ToList(),
                    Dimensions = new ItemDimensionsEntity
                    {
                        Depth = dims.Depth,
                        Height = dims.Height,
                        MeasureUnitId = (int)dims.MeasureUnit,
                        Weight = dims.Weight,
                        Width = dims.Width
                    },
                    Deliveries = info.RentItem.DeliveryTypes.Select(x => new ItemDeliveryEntity
                    {
                        DeliveryTypeId = (int)x,
                    }).ToList()
                },
                Location = new LocationEntity
                {
                    CityId = info.Location.CityId,
                    Address = info.Location.Address,
                },
                RentConfiguration = new List<RentConfigurationEntity>
                                                   {
                                                       new RentConfigurationEntity
                                                           {
                                                               Id = rentConfig.Id,
                                                               AdvertisementId = info.Id,
                                                               FullPricePerPeriod = rentConfig.FullPricePerPeriod,
                                                               CurrencyId = (int)rentConfig.Currency,
                                                               MaxRentDuration = rentConfig.MaxRentDuration,
                                                               MinRentDuration = rentConfig.MinRentDuration,
                                                               RentPeriodId = (int)rentConfig.RentPeriod,
                                                           }
                                                   }
            };
        }
    }
}