namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        UserId = c.String(),
                        StatusId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdvertisementStatus", t => t.StatusId, cascadeDelete: true)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.RentItems",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        ItemCategoryId = c.Int(nullable: false),
                        GenderId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisements", t => t.Id)
                .ForeignKey("dbo.GenderEntities", t => t.GenderId)
                .ForeignKey("dbo.ItemCategories", t => t.ItemCategoryId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.ItemCategoryId)
                .Index(t => t.GenderId);
            
            CreateTable(
                "dbo.BookingRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RentItemId = c.Int(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        RequesterUserId = c.String(),
                        StatusId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentItems", t => t.RentItemId, cascadeDelete: true)
                .ForeignKey("dbo.BookingRequestStatus", t => t.StatusId, cascadeDelete: true)
                .Index(t => t.RentItemId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.BookingRequestStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemDeliveries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryTypeId = c.Int(nullable: false),
                        RentItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeliveryTypes", t => t.DeliveryTypeId, cascadeDelete: true)
                .ForeignKey("dbo.RentItems", t => t.RentItemId, cascadeDelete: true)
                .Index(t => t.DeliveryTypeId)
                .Index(t => t.RentItemId);
            
            CreateTable(
                "dbo.DeliveryTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Price = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemWeights",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Depth = c.Single(),
                        Height = c.Single(nullable: false),
                        Weight = c.Single(),
                        Width = c.Single(nullable: false),
                        MeasureUnitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeasureUnits", t => t.MeasureUnitId, cascadeDelete: true)
                .ForeignKey("dbo.RentItems", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.MeasureUnitId);
            
            CreateTable(
                "dbo.MeasureUnits",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GenderEntities",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemCategories",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemPackages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ObjectName = c.String(),
                        Quantity = c.Int(nullable: false),
                        RentItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentItems", t => t.RentItemId, cascadeDelete: true)
                .Index(t => t.RentItemId);
            
            CreateTable(
                "dbo.ItemPhotos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CloudUrl = c.String(),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        IsThumbnail = c.Boolean(nullable: false),
                        Name = c.String(),
                        RentItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentItems", t => t.RentItemId, cascadeDelete: true)
                .Index(t => t.RentItemId);
            
            CreateTable(
                "dbo.RentItemProperties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                        RentItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentItems", t => t.RentItemId, cascadeDelete: true)
                .Index(t => t.RentItemId);
            
            CreateTable(
                "dbo.AdvertisementLocations",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Address = c.String(),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisements", t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Native = c.String(),
                        Phone = c.String(),
                        Currency = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RentConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AdvertisementId = c.Int(nullable: false),
                        FullPricePerPeriod = c.Single(nullable: false),
                        MaxRentDuration = c.Int(),
                        MinRentDuration = c.Int(),
                        RentPeriodId = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisements", t => t.AdvertisementId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .ForeignKey("dbo.RentPeriods", t => t.RentPeriodId, cascadeDelete: true)
                .Index(t => t.AdvertisementId)
                .Index(t => t.RentPeriodId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DiscountSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RentConfigurationId = c.Int(nullable: false),
                        FromInclusive = c.Int(nullable: false),
                        ToInclusive = c.Int(nullable: false),
                        Discount = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentConfigurations", t => t.RentConfigurationId, cascadeDelete: true)
                .Index(t => t.RentConfigurationId);
            
            CreateTable(
                "dbo.RentPeriods",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AdvertisementStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Advertisements", "StatusId", "dbo.AdvertisementStatus");
            DropForeignKey("dbo.RentConfigurations", "RentPeriodId", "dbo.RentPeriods");
            DropForeignKey("dbo.DiscountSettings", "RentConfigurationId", "dbo.RentConfigurations");
            DropForeignKey("dbo.RentConfigurations", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.RentConfigurations", "AdvertisementId", "dbo.Advertisements");
            DropForeignKey("dbo.AdvertisementLocations", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.AdvertisementLocations", "Id", "dbo.Advertisements");
            DropForeignKey("dbo.RentItemProperties", "RentItemId", "dbo.RentItems");
            DropForeignKey("dbo.ItemPhotos", "RentItemId", "dbo.RentItems");
            DropForeignKey("dbo.ItemPackages", "RentItemId", "dbo.RentItems");
            DropForeignKey("dbo.RentItems", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.RentItems", "GenderId", "dbo.GenderEntities");
            DropForeignKey("dbo.ItemWeights", "Id", "dbo.RentItems");
            DropForeignKey("dbo.ItemWeights", "MeasureUnitId", "dbo.MeasureUnits");
            DropForeignKey("dbo.ItemDeliveries", "RentItemId", "dbo.RentItems");
            DropForeignKey("dbo.ItemDeliveries", "DeliveryTypeId", "dbo.DeliveryTypes");
            DropForeignKey("dbo.BookingRequests", "StatusId", "dbo.BookingRequestStatus");
            DropForeignKey("dbo.BookingRequests", "RentItemId", "dbo.RentItems");
            DropForeignKey("dbo.RentItems", "Id", "dbo.Advertisements");
            DropIndex("dbo.DiscountSettings", new[] { "RentConfigurationId" });
            DropIndex("dbo.RentConfigurations", new[] { "CurrencyId" });
            DropIndex("dbo.RentConfigurations", new[] { "RentPeriodId" });
            DropIndex("dbo.RentConfigurations", new[] { "AdvertisementId" });
            DropIndex("dbo.Cities", new[] { "CountryId" });
            DropIndex("dbo.AdvertisementLocations", new[] { "CityId" });
            DropIndex("dbo.AdvertisementLocations", new[] { "Id" });
            DropIndex("dbo.RentItemProperties", new[] { "RentItemId" });
            DropIndex("dbo.ItemPhotos", new[] { "RentItemId" });
            DropIndex("dbo.ItemPackages", new[] { "RentItemId" });
            DropIndex("dbo.ItemWeights", new[] { "MeasureUnitId" });
            DropIndex("dbo.ItemWeights", new[] { "Id" });
            DropIndex("dbo.ItemDeliveries", new[] { "RentItemId" });
            DropIndex("dbo.ItemDeliveries", new[] { "DeliveryTypeId" });
            DropIndex("dbo.BookingRequests", new[] { "StatusId" });
            DropIndex("dbo.BookingRequests", new[] { "RentItemId" });
            DropIndex("dbo.RentItems", new[] { "GenderId" });
            DropIndex("dbo.RentItems", new[] { "ItemCategoryId" });
            DropIndex("dbo.RentItems", new[] { "Id" });
            DropIndex("dbo.Advertisements", new[] { "StatusId" });
            DropTable("dbo.AdvertisementStatus");
            DropTable("dbo.RentPeriods");
            DropTable("dbo.DiscountSettings");
            DropTable("dbo.Currencies");
            DropTable("dbo.RentConfigurations");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
            DropTable("dbo.AdvertisementLocations");
            DropTable("dbo.RentItemProperties");
            DropTable("dbo.ItemPhotos");
            DropTable("dbo.ItemPackages");
            DropTable("dbo.ItemCategories");
            DropTable("dbo.GenderEntities");
            DropTable("dbo.MeasureUnits");
            DropTable("dbo.ItemWeights");
            DropTable("dbo.DeliveryTypes");
            DropTable("dbo.ItemDeliveries");
            DropTable("dbo.BookingRequestStatus");
            DropTable("dbo.BookingRequests");
            DropTable("dbo.RentItems");
            DropTable("dbo.Advertisements");
        }
    }
}
