namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Advertisements.Service.DataAccess.Context.Context>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context.Context context)
        {
            foreach (Currency currency in Enum.GetValues(typeof(Currency)))
            {
                context.Currencies.AddOrUpdate(new CurrencyEntity { Id = (int)currency, Name = currency.ToString() });
            }

            foreach (ItemCategory category in Enum.GetValues(typeof(ItemCategory)))
            {
                context.Categories.AddOrUpdate(
                    new ItemCategoryEntity { Id = (int)category, Name = category.ToString() });
            }

            foreach (RentPeriod rentPeriod in Enum.GetValues(typeof(RentPeriod)))
            {
                context.RentPeriods.AddOrUpdate(
                    new RentPeriodEntity { Id = (int)rentPeriod, Name = rentPeriod.ToString() });
            }

            foreach (AdvertisementStatus status in Enum.GetValues(typeof(AdvertisementStatus)))
            {
                context.AdvertisementStatus.AddOrUpdate(
                    new AdvertisementStatusEntity { Id = (int)status, Name = status.ToString() });
            }

            foreach (DeliveryType deliveryType in Enum.GetValues(typeof(DeliveryType)))
            {
                context.DeliveryTypes.AddOrUpdate(
                    new DeliveryTypeEntity { Id = (int)deliveryType, Name = deliveryType.ToString() });
            }

            foreach (Gender gender in Enum.GetValues(typeof(Gender)))
            {
                context.Genders.AddOrUpdate(
                    new GenderEntity { Id = (int)gender, Name = gender.ToString() });
            }

            foreach (MeasureUnit unit in Enum.GetValues(typeof(MeasureUnit)))
            {
                context.MeasureUnits.AddOrUpdate(
                    new MeasureUnitEntity { Id = (int)unit, Name = unit.ToString() });
            }

            foreach (BookingRequestStatus status in Enum.GetValues(typeof(BookingRequestStatus)))
            {
                context.BookingRequestStatus.AddOrUpdate(
                    new BookingRequestStatusEntity { Id = (int)status, Name = status.ToString() });
            }

            //using (var reader = new StreamReader(@"D:\Projects\Startup\ActiveUse\RentalApp\Advertisements.Service.DataAccess\bin\Debug\Data\countries.json"))
            //{
            //    dynamic data = JObject.Parse(reader.ReadToEnd());
            //    foreach (var x in data)
            //    {
            //        JToken value = x.Value;

            //        var country = JsonConvert.DeserializeObject<CountryEntity>(value.ToString());
            //        context.Countries.AddOrUpdate(country);
            //    }
            //}

            //using (var reader = new StreamReader(
            //    @"D:\Projects\Startup\ActiveUse\RentalApp\Advertisements.Service.DataAccess\Data\countriesandcities.json"))
            //{
            //    dynamic data = JObject.Parse(reader.ReadToEnd());
            //    foreach (var x in data)
            //    {
            //        string name = x.Name;
            //        JToken value = x.Value;
            //        var country = context.Countries
            //            .Include("Cities")
            //            .FirstOrDefault(p => p.Name == name);
            //        if (country != null)
            //        {
            //            var cities = JsonConvert.DeserializeObject<string[]>(value.ToString());
            //            foreach (var city in cities)
            //            {
            //                country.Cities.Add(new CityEntity { Name = city });
            //            }
            //        }
            //    }
            //}

            context.SaveChanges();
        }
    }
}