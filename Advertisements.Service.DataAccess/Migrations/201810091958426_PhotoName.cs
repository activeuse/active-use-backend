namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotoName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemPhotos", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemPhotos", "Name");
        }
    }
}
