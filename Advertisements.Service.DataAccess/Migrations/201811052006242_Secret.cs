namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Secret : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BookingRequests", "SecretPhrase", c => c.String());
            AlterColumn("dbo.RentConfigurations", "FullPricePerPeriod", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RentConfigurations", "FullPricePerPeriod", c => c.Single(nullable: false));
            DropColumn("dbo.BookingRequests", "SecretPhrase");
        }
    }
}
