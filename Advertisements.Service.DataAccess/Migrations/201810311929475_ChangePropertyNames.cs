namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePropertyNames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RentItems", "Details", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RentItems", "Details");
        }
    }
}
