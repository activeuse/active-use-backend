namespace Advertisements.Service.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeContext : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ItemPhotos", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ItemPhotos", "Name", c => c.String());
        }
    }
}
