﻿namespace Advertisements.Service.DataAccess.Models
{
    using global::Service.Core.Entities.Enums;

    public class ItemDeliveryInfoModel
    {
        public DeliveryType DeliveryType { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }
    }
}
