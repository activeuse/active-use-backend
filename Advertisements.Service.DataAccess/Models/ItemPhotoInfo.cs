﻿namespace Advertisements.Service.DataAccess.Models
{
    public class ItemPhotoInfo
    {
        public string CloudUrl { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public bool IsThumbnail { get; set; }

        public string Name { get; set; }
    }
}
