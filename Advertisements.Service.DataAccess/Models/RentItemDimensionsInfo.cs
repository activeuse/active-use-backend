﻿namespace Advertisements.Service.DataAccess.Models
{
    using global::Service.Core.Entities.Enums;

    public sealed class RentItemDimensionsInfo
    {
        public float? Depth { get; set; }

        public float Height { get; set; }

        public MeasureUnit MeasureUnit { get; set; }

        public float? Weight { get; set; }

        public float Width { get; set; }
    }
}