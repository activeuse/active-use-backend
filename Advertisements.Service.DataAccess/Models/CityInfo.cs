﻿namespace Advertisements.Service.DataAccess.Models
{
    using global::Service.Core.Entities;

    public sealed class CityInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
