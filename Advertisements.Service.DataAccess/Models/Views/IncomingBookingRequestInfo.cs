﻿namespace Advertisements.Service.DataAccess.Models.Views
{
    using System;

    using global::Service.Core.Entities.Enums;

    public class IncomingBookingRequestInfo
    {
        public int Id { get; set; }

        public AdvertisementInfo Advertisement { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }

        public BookingRequestStatus Status { get; set; }
    }
}