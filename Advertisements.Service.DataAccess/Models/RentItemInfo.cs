﻿namespace Advertisements.Service.DataAccess.Models
{
    using System.Collections.Generic;

    using global::Service.Core.Entities.Enums;

    public sealed class RentItemInfo
    {
        public string Name { get; set; }

        public string Details { get; set; }

        public Gender? Gender { get; set; }

        public ItemCategory ItemCategory { get; set; }

        public List<ItemPhotoInfo> Photos { get; set; }

        public RentItemDimensionsInfo Dimensions { get; set; }

        public List<ItemPackageInfo> Package { get; set; }

        public List<RentItemPropertyInfo> Properties { get; set; }

        public List<ItemDeliveryInfoModel> DeliveryTypes { get; set; }
    }
}