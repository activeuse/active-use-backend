﻿namespace Advertisements.Service.DataAccess.Models
{
    public sealed class CountryInfo
    {
        public int Id { get; set; }

        public string Currency { get; set; }

        public string Name { get; set; }

        public string NativeCountryName { get; set; }

        public string Phone { get; set; }
    }
}