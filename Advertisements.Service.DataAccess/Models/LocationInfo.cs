﻿namespace Advertisements.Service.DataAccess.Models
{
    public sealed class LocationInfo
    {
        public string Address { get; set; }

        public int CityId { get; set; }

        public string Country { get; set; }

        public int CountryId { get; set; }

        public string City { get; set; }

        public string NativeCountryName { get; set; }
    }
}