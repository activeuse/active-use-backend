﻿namespace Advertisements.Service.DataAccess.Models
{
    using System;
    using System.Collections.Generic;

    using global::Service.Core.Entities.Enums;

    public sealed class AdvertisementInfo
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public ItemPhotoInfo CoverPhoto { get; set; }

        public RentItemInfo RentItem { get; set; }

        public RentConfigurationInfo RentConfiguration { get; set; }

        public AdvertisementStatus Status { get; set; }

        public LocationInfo Location { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}