﻿namespace Advertisements.Service.DataAccess.Models
{
    public sealed class DiscountSettingsInfo
    {
        public float Discount { get; set; }

        public int FromInclusive { get; set; }

        public int ToInclusive { get; set; }
    }
}