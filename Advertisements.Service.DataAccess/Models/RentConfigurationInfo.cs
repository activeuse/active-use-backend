﻿namespace Advertisements.Service.DataAccess.Models
{
    using global::Service.Core.Entities.Enums;

    public sealed class RentConfigurationInfo
    {
        public float FullPricePerPeriod { get; set; }

        public int? MaxRentDuration { get; set; }

        public int? MinRentDuration { get; set; }

        public RentPeriod RentPeriod { get; set; }

        public Currency Currency { get; set; }
    }
}