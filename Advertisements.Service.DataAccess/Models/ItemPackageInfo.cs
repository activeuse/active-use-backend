﻿namespace Advertisements.Service.DataAccess.Models
{
    public sealed class ItemPackageInfo
    {
        public string ObjectName { get; set; }

        public int Quantity { get; set; }
    }
}