﻿namespace Advertisements.Service.DataAccess.Models
{
    public class RentItemPropertyInfo
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}