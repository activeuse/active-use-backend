﻿namespace Advertisements.Service.DataAccess.Models
{
    public sealed class FileUploadInfo
    {
        public byte[] Data { get; set; }

        public string Name { get; set; }
    }
}