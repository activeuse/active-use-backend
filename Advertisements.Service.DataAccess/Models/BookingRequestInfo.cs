﻿namespace Advertisements.Service.DataAccess.Models
{
    using System;

    public sealed class BookingRequestInfo
    {
        public DateTime EndDate { get; set; }

        public int RentItemId { get; set; }

        public string RequesterUserId { get; set; }

        public DateTime StartDate { get; set; }

    }
}