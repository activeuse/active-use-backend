﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Context;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    internal sealed class AdvertisementRepository : IAdvertisementRepository
    {
        public async Task<int> GetAdvertisementsCount(string userId)
        {
            using (var context = this.GetContext())
            {
                return await context.Advertisements.CountAsync(x => x.UserId == userId);
            }
        }

        public async Task<AdvertisementEntity> Get(int id, string userId)
        {
            using (var context = this.GetContext())
            {
                return await context.Advertisements.AsNoTracking()
                           .Include(x => x.RentItem) 
                           .Include(x => x.RentItem.Gender) 
                           .Include(x => x.RentItem.Photos)
                           .Include(x => x.RentItem.ItemCategory)
                           .Include(x => x.RentItem.Package)
                           .Include(x => x.RentItem.Deliveries)
                           .Include(x => x.RentItem.Deliveries.Select(p => p.DeliveryType))
                           .Include(x => x.RentItem.Dimensions)
                           .Include(x => x.RentItem.Dimensions.MeasureUnit)
                           .Include(x => x.RentItem.Properties)
                           .Include(x => x.Status)
                           .Include(x => x.RentConfiguration)
                           .Include(x => x.RentConfiguration.Select(p => p.RentPeriod))
                           .Include(x => x.RentConfiguration.Select(p => p.Currency))
                           .Include(x => x.RentConfiguration.Select(p => p.DiscountSettings))
                           .Include(x => x.Location)
                           .Include(x => x.Location.City)
                           .Include(x => x.Location.City.Country)
                    .FirstOrDefaultAsync(x => x.Id == id);
            }
        }

        public async Task<List<AdvertisementEntity>> Get(int page, int take, string userId)
        {
            using (var context = this.GetContext())
            {
                return await context.Advertisements.AsNoTracking()
                           .Include(x => x.RentItem)
                           .Include(x => x.RentItem.Gender)
                           .Include(x => x.RentItem.Photos)
                           .Include(x => x.RentItem.ItemCategory)
                           .Include(x => x.RentItem.Package)
                           .Include(x => x.RentItem.Deliveries)
                           .Include(x => x.RentItem.Deliveries.Select(p => p.DeliveryType))
                           .Include(x => x.RentItem.Dimensions)
                           .Include(x => x.RentItem.Dimensions.MeasureUnit)
                           .Include(x => x.RentItem.Properties)
                           .Include(x => x.Status)
                           .Include(x => x.RentConfiguration)
                           .Include(x => x.RentConfiguration.Select(p => p.RentPeriod))
                           .Include(x => x.RentConfiguration.Select(p => p.Currency))
                           .Include(x => x.RentConfiguration.Select(p => p.DiscountSettings))
                           .Include(x => x.Location)
                           .Include(x => x.Location.City)
                           .Include(x => x.Location.City.Country)
                           .Where(x => x.UserId == userId)
                           .OrderByDescending(x => x.CreatedAt)
                           .Skip(page * take)
                           .Take(take)
                           .ToListAsync();
            }
        }

        public async Task<List<AdvertisementEntity>> GetWithStatus(AdvertisementStatus status, string userId)
        {
            using (var context = this.GetContext())
            {
                return await context.Advertisements.AsNoTracking()
                           .Include(x => x.RentItem)
                           .Include(x => x.RentItem.Gender)
                           .Include(x => x.RentItem.Photos)
                           .Include(x => x.RentItem.ItemCategory)
                           .Include(x => x.RentItem.Package)
                           .Include(x => x.RentItem.Deliveries)
                           .Include(x => x.RentItem.Deliveries.Select(p => p.DeliveryType))
                           .Include(x => x.RentItem.Dimensions)
                           .Include(x => x.RentItem.Dimensions.MeasureUnit)
                           .Include(x => x.RentItem.Properties)
                           .Include(x => x.Status)
                           .Include(x => x.RentConfiguration)
                           .Include(x => x.RentConfiguration.Select(p => p.RentPeriod))
                           .Include(x => x.RentConfiguration.Select(p => p.Currency))
                           .Include(x => x.RentConfiguration.Select(p => p.DiscountSettings))
                           .Include(x => x.Location)
                           .Include(x => x.Location.City)
                           .Include(x => x.Location.City.Country)
                           .Where(x => x.UserId == userId && x.Status.Id == (int)status)
                           .ToListAsync();
            }
        }

        public async Task<List<AdvertisementEntity>> Search(
            Expression<Func<AdvertisementEntity, bool>> searchExpression, int skip, int take)
        {
            using (var context = this.GetContext())
            {
                return await context.Advertisements.AsNoTracking().Include(x => x.RentItem).Include(x => x.RentItem.Gender)
                           .Include(x => x.RentItem.Photos).Include(x => x.RentItem.ItemCategory).Include(x => x.RentItem.Package)
                           .Include(x => x.RentItem.Deliveries).Include(x => x.RentItem.Deliveries.Select(p => p.DeliveryType))
                           .Include(x => x.RentItem.Dimensions).Include(x => x.RentItem.Properties).Include(x => x.Status)
                           .Include(x => x.RentConfiguration)
                           .Include(x => x.RentConfiguration.Select(p => p.RentPeriod))
                           .Include(x => x.RentConfiguration.Select(p => p.Currency))
                           .Include(x => x.RentConfiguration.Select(p => p.DiscountSettings)).Include(x => x.Location)
                           .Include(x => x.Location.City).Include(x => x.Location.City.Country)
                           .Where(searchExpression)
                           .OrderByDescending(x => x.CreatedAt)
                           .Skip(skip).Take(take).ToListAsync();
            }
        }

        public async Task Delete(int id, string userId)
        {
            using (var context = this.GetContext())
            {
                var ad = await context.Advertisements.FirstOrDefaultAsync(x => x.Id == id && x.UserId == userId);
                if (ad == null)
                {
                    return;
                }

                ad.StatusId = (int)AdvertisementStatus.Deleted;
                await context.SaveChangesAsync();
            }
        }

        public async Task Update(AdvertisementEntity entity)
        {
            using (var context = this.GetContext())
            {
                foreach (var photo in entity.RentItem.Photos)
                {
                    context.Entry(photo).State = EntityState.Added;
                    entity.UpdatedAt = DateTime.UtcNow;
                }

                context.Entry(entity.RentItem).State = EntityState.Modified;

                foreach (var rentConfiguration in entity.RentConfiguration)
                {
                    context.Entry(rentConfiguration).State = EntityState.Modified;
                }

                context.Entry(entity).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }
        }

        public async Task<int> Create(AdvertisementEntity advertisement)
        {
            using (var context = this.GetContext())
            {
                context.Advertisements.Add(advertisement);

                await context.SaveChangesAsync();

                return advertisement.Id;
            }
        }

        private Context GetContext()
        {
            return new Context();
        }
    }
}
