﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::Service.Core;

    public interface IBasicRepository<TEntity>
        where TEntity : class, BaseIdentity<int>
    {
        Task<TEntity> Get(int id);

        Task<List<TEntity>> GetAll();

        Task Update(TEntity entity);

        Task Delete(int id);

        Task Create(TEntity entity);

        Task DeleteRange(List<TEntity> entities);
    }
}