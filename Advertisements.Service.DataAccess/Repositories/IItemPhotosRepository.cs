﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::Service.Core.Entities;

    internal interface IItemPhotosRepository
    {
        Task<List<ItemPhotoEntity>> GetByRentItemId(int rentItemId);

        Task<ItemPhotoEntity> Get(int id);

        Task<List<ItemPhotoEntity>> GetAll();

        Task Update(ItemPhotoEntity entity);

        Task Delete(int id);

        Task DeleteRange(List<ItemPhotoEntity> entities);

        Task Create(ItemPhotoEntity entity);
    }
}