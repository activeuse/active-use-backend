﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Context;

    using global::Service.Core.Entities;

    internal sealed class LocationsRepository : ILocationsRepository
    {
        public async Task<List<CityEntity>> GetCities(int countryId)
        {
            using (var context = new Context())
            {
                return await context.Cities.Where(x => x.CountryId == countryId)
                    .ToListAsync();
            }
        }

        public async Task<IEnumerable<CountryEntity>> GetCountries()
        {
            using (var context = new Context())
            {
                return await context.Countries.ToListAsync();
            }
        }
    }
}