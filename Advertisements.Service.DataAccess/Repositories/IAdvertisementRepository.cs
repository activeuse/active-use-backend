﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    internal interface IAdvertisementRepository
    {
        Task<int> GetAdvertisementsCount(string userId);

        Task<AdvertisementEntity> Get(int id, string userId);

        Task<List<AdvertisementEntity>> Get(int page, int take, string userId);

        Task<List<AdvertisementEntity>> GetWithStatus(AdvertisementStatus status, string userId);

        Task<int> Create(AdvertisementEntity advertisement);

        Task Delete(int id, string userId);

        Task Update(AdvertisementEntity entity);

        Task<List<AdvertisementEntity>> Search(Expression<Func<AdvertisementEntity, bool>> searchExpression, int skip, int take);
    }
}