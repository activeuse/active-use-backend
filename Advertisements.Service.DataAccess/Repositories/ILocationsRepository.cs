﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::Service.Core.Entities;

    internal interface ILocationsRepository
    {
        Task<List<CityEntity>> GetCities(int countryId);

        Task<IEnumerable<CountryEntity>> GetCountries();
    }
}