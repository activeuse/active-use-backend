﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Context;

    using global::Service.Core.Entities;

    internal sealed class ItemPhotosRepository : BasicRepository<ItemPhotoEntity>, IItemPhotosRepository
    {
        public async Task<List<ItemPhotoEntity>> GetByRentItemId(int rentItemId)
        {
            using (var context = new Context())
            {
                return await context.ItemPhotos.Where(x => x.RentItemId == rentItemId).ToListAsync();
            }
        }
    }
}