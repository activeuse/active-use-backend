﻿namespace Advertisements.Service.DataAccess.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Context;

    using global::Service.Core;

    internal class BasicRepository<TEntity> : IBasicRepository<TEntity>
        where TEntity : class, BaseIdentity<int>
    {
        public async Task<TEntity> Get(int id)
        {
            using (var context = new Context())
            {
                return await context.Set<TEntity>().FindAsync(id);
            }
        }

        public async Task<List<TEntity>> GetAll()
        {
            using (var context = new Context())
            {
                return await context.Set<TEntity>().AsNoTracking().ToListAsync();
            }
        }

        public async Task Update(TEntity entity)
        {
            using (var context = new Context())
            {
                var itemExists = context.Set<TEntity>().Find(entity.Id) == null;
                if (!itemExists)
                {
                    return;
                }

                context.Set<TEntity>().AddOrUpdate(entity);
                await context.SaveChangesAsync();
            }
        }

        public async Task Delete(int id)
        {
            using (var context = new Context())
            {
               var item = context.Set<TEntity>().Find(id);
                if (item == null)
                {
                    return;
                }

                context.Set<TEntity>().Remove(item);

                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteRange(List<TEntity> entities)
        {
            using (var context = new Context())
            {
                foreach (var entity in entities)
                {
                    context.Entry(entity).State = EntityState.Unchanged;
                }

                context.Set<TEntity>().RemoveRange(entities);
                await context.SaveChangesAsync();
            }
        }

        public async Task Create(TEntity entity)
        {
            using (var context = new Context())
            {
                context.Set<TEntity>().Add(entity);
                await context.SaveChangesAsync();
            }
        }
    }
}