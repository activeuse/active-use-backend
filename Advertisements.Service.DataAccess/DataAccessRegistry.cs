﻿namespace Advertisements.Service.DataAccess
{
    using Advertisements.Service.DataAccess.Repositories;
    using Advertisements.Service.DataAccess.Services;

    using AutoMapper;

    using Payments.Service.DataAccess;

    using StructureMap;

    public sealed class DataAccessRegistry : Registry
    {
        public DataAccessRegistry()
        {
            this.RegisterRepositories();
            this.RegisterServices();
        }

        private void RegisterRepositories()
        {
            this.For<IAdvertisementRepository>().Use<AdvertisementRepository>();
            this.For(typeof(IBasicRepository<>)).Use(typeof(BasicRepository<>));
            this.For<IItemPhotosRepository>().Use<ItemPhotosRepository>();
            this.For<ILocationsRepository>().Use<LocationsRepository>();
        }

        private void RegisterServices()
        {
            this.For<IAdvertisementsSearchService>().Use<AdvertisementsSearchService>();
            this.For<IAdvertisementsService>().Use<AdvertisementsService>();
            this.For<IBookingService>().Use<BookingService>();
            this.For<ILocationsService>().Use<LocationsService>();
        }
    }
}