﻿namespace Advertisements.Service.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Context;
    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Models.Views;
    using Advertisements.Service.DataAccess.Repositories;

    using AutoMapper;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;
    using global::Service.Core.Entities.Views;
    using global::Service.Core.Exceptions;
    using global::Service.Core.Logging;
    using global::Service.Core.Security;
    using global::Service.Core.Sql;

    using Payments.Service.DataAccess.Models;
    using Payments.Service.DataAccess.Services;

    internal sealed class BookingService : BaseDataAccess, IBookingService
    {
        private static readonly Semaphore Semaphore = new Semaphore(1, 1);

        private readonly int maxNumberOfActiveRequests;

        private readonly IMapper mapper;

        private readonly IAdvertisementRepository advertisementsRepository;

        private readonly IDatabaseLogger databaseLogger;

        private readonly IEvtWalletsService evtWalletsService;

        private readonly ISecretHelper secretHelper;

        public BookingService(IMapper mapper, IAdvertisementRepository advertisementsRepository, IDatabaseLogger databaseLogger, IEvtWalletsService evtWalletsService, ISecretHelper secretHelper)
        {
            this.mapper = mapper;
            this.advertisementsRepository = advertisementsRepository;
            this.databaseLogger = databaseLogger;
            this.evtWalletsService = evtWalletsService;
            this.secretHelper = secretHelper;
            this.maxNumberOfActiveRequests = int.Parse(ConfigurationManager.AppSettings["maxActiveRequestPerUser"]);
        }

        /// <inheritdoc />
        protected override string ConnectionStringName { get; set; } = "Advertisements";

        public async Task RejectRequest(int requestId, string userId)
        {
            var requestView = await this.GetIncomingRequest(requestId, userId);
            if (requestView == null)
            {
                throw new ItemNotFoundException("Booking request not found.");
            }

            if (requestView.BookingRequestStatus != (int)BookingRequestStatus.New)
            {
                throw new EntityBadStateException("Can's reject request.");
            }

            await this.ChangeRequestStatus(requestId, BookingRequestStatus.Rejected);

            await this.databaseLogger.LogEvent(
                LogType.StatusChange,
                new { Operation = "Reject Incoming Request", RequestId = requestId, UserId = userId });
        }

        public async Task ApproveRequest(int requestId, string userId)
        {
            var requestView = await this.GetIncomingRequest(requestId, userId);
            if (requestView == null)
            {
                throw new ItemNotFoundException("Booking request not found.");
            }

            if (requestView.BookingRequestStatus != (int)BookingRequestStatus.New)
            {
                throw new EntityBadStateException("Can's approve not new booking request.");
            }

            var advertisement = await this.advertisementsRepository.Get(
                                    requestView.AdvertisementId,
                                    requestView.AdvertisementOwnerUserId);

            var secret = this.secretHelper.Decrypt(requestView.SecretPhrase);

            var requesterWallet = await this.evtWalletsService.GetActiveWallet(secret, requestView.RequesterUserId);
            if (requesterWallet == null)
            {
                throw new ItemNotFoundException("Wallet issue");
            }

            var totalDays = requestView.EndDateTime.Subtract(requestView.StartDateTime).TotalDays;
            var requiredBalance = (decimal)totalDays * advertisement.RentConfiguration.First().FullPricePerPeriod;

            if (requesterWallet.Balance < requiredBalance)
            {
                throw new EntityBadStateException("Insufficient balance");
            }

            var receiverWallets = await this.evtWalletsService.GetAllWallets(requestView.AdvertisementOwnerUserId);
            if (!receiverWallets.Any())
            {
                throw new EntityBadStateException("Advertisement owner does not have wallets");
            }

            await this.ChangeRequestStatus(requestId, BookingRequestStatus.Approved);

            using (var context = new Context())
            {
                var bookingRequest =
                    context.BookingRequests.Where(x => x.RentItemId == requestView.AdvertisementId && x.StatusId == (int)BookingRequestStatus.New);

                var overlappingRequests = bookingRequest.Where(booking => booking.StartDateTime <= requestView.EndDateTime && requestView.StartDateTime <= booking.EndDateTime).ToList();

                foreach (var request in overlappingRequests)
                {
                    await this.ChangeRequestStatus(request.Id, BookingRequestStatus.Rejected);
                }
            }

            await this.evtWalletsService.Transfer(
                requiredBalance,
                requestId,
                receiverWallets.First(x => x.IsActive).Address,
                secret,
                "Transfer for item",
                requestView.RequesterUserId);
        }

        public async Task DeleteOutgoingRequest(int requestId, string userId)
        {
            var requestView = await this.GetOutgoingRequest(requestId, userId);
            if (requestView == null)
            {
                throw new ItemNotFoundException("Booking request not found.");
            }

            if (requestView.BookingRequestStatus != (int)BookingRequestStatus.New)
            {
                throw new EntityBadStateException("Can's delete not new booking request.");
            }

            await this.DeleteOutgoingRequestInternal(requestId);
        }

        public async Task<List<OutgoingBookingRequestInfo>> GetOutgoingRequests(string userId)
        {
            const string Query = "select * from [BookingRequestsView] where [RequesterUserId] = @userId;";

            var bookings = await this.RunMsSqlQuery(
                       Query,
                       async command => await command.ReadItems(this.ReadBookingRequestView),
                       p => { p.AddWithValue("userId", userId); });

            var result = new List<OutgoingBookingRequestInfo>();

            foreach (var booking in bookings)
            {
                var relatedAdvertisement = await this.advertisementsRepository.Get(
                                               booking.AdvertisementId,
                                               booking.AdvertisementOwnerUserId);

                var incomingBookingRequest = new OutgoingBookingRequestInfo
                {
                    Id = booking.BookingRequestId,
                    Advertisement = this.mapper.Map<AdvertisementInfo>(relatedAdvertisement),
                    Status = (BookingRequestStatus)booking.BookingRequestStatus,
                    EndDate = booking.EndDateTime,
                    StartDate = booking.StartDateTime
                };

                result.Add(incomingBookingRequest);
            }

            return result;
        }

        public async Task<List<IncomingBookingRequestInfo>> GetIncomingRequests(string userId)
        {
            const string Query = "select * from [BookingRequestsView] where [AdvertisementOwnerUserId] = @userId;";

            var bookings = await this.RunMsSqlQuery(
                               Query,
                               async command => await command.ReadItems(this.ReadBookingRequestView),
                               p => { p.AddWithValue("userId", userId); });

            var result = new List<IncomingBookingRequestInfo>();

            foreach (var booking in bookings)
            {
                var relatedAdvertisement = await this.advertisementsRepository.Get(
                                                                          booking.AdvertisementId,
                                                                          booking.AdvertisementOwnerUserId);

                var incomingBookingRequest = new IncomingBookingRequestInfo
                {
                    Id = booking.BookingRequestId,
                    Advertisement = this.mapper.Map<AdvertisementInfo>(relatedAdvertisement),
                    Status = (BookingRequestStatus)booking.BookingRequestStatus,
                    EndDate = booking.EndDateTime,
                    StartDate = booking.StartDateTime
                };

                result.Add(incomingBookingRequest);
            }

            return result;
        }

        public async Task<BookingRequestResult> Request(BookingRequestInfo request, string secretPhrase)
        {
            var itemAvailable = await this.CheckItemAvailable(request.RentItemId, request.StartDate, request.EndDate);
            if (!itemAvailable)
            {
                return BookingRequestResult.DatesNotAvailable;
            }

            using (var context = new Context())
            {
                var advertisement = await context.Advertisements
                                   .AsNoTracking()
                                   .Include(x => x.RentItem)
                                   .Include(x => x.RentConfiguration)
                                   .FirstOrDefaultAsync(x => x.Id == request.RentItemId);

                if (advertisement == null)
                {
                    return BookingRequestResult.AdvertisementDoesNotExist;
                }

                if (advertisement.StatusId != (int)AdvertisementStatus.Active)
                {
                    return BookingRequestResult.AdvertisementDoesNotExist;
                }

                if (advertisement.UserId == request.RequesterUserId)
                {
                    return BookingRequestResult.CantBookOwnItem;
                }

                var activeUserRequests = context.BookingRequests
                                         .AsNoTracking()
                                         .Where(x => x.RequesterUserId == request.RequesterUserId &&
                                                        (x.StatusId == (int)BookingRequestStatus.New || x.StatusId == (int)BookingRequestStatus.Approved));

                if (await activeUserRequests.AnyAsync(x => x.RequesterUserId == request.RequesterUserId && x.RentItemId == request.RentItemId))
                {
                    return BookingRequestResult.AlreadyExists;
                }

                var activeUserRequestsCount = await activeUserRequests.CountAsync();

                if (activeUserRequestsCount > this.maxNumberOfActiveRequests)
                {
                    return BookingRequestResult.MaxNumberOfActiveRequestsExceeded;
                }

                var activeUserWallet = await this.evtWalletsService.GetActiveWallet(secretPhrase, request.RequesterUserId);
                if (activeUserWallet == null)
                {
                    throw new ItemNotFoundException("User does not have active wallet matching secret phrase");
                }

                var totalDays = request.EndDate.Subtract(request.StartDate).TotalDays;
                var requiredBalance = (decimal)totalDays * advertisement.RentConfiguration.First().FullPricePerPeriod;
                if (activeUserWallet.Balance < requiredBalance)
                {
                    return BookingRequestResult.InsufficientBalance;
                }
            }

            try
            {
                Semaphore.WaitOne();
                using (var context = new Context())
                {
                    context.BookingRequests.Add(
                        new BookingRequestEntity
                        {
                            RentItemId = request.RentItemId,
                            StartDateTime = request.StartDate,
                            EndDateTime = request.EndDate,
                            RequesterUserId = request.RequesterUserId,
                            SecretPhrase = this.secretHelper.Encrypt(secretPhrase),
                            StatusId = (int)BookingRequestStatus.New
                        });

                    await context.SaveChangesAsync();
                }
            }
            finally
            {
                Semaphore.Release();
            }

            return BookingRequestResult.Success;
        }

        private async Task<RentItemBookingData> GetIncomingRequest(int requestId, string userId)
        {
            const string Query = "select * from [BookingRequestsView] where [AdvertisementOwnerUserId] = @userId and [BookingRequestId] = @requestId;";

            var booking = await this.RunMsSqlQuery(
                              Query,
                              async command => await command.ReadItem(this.ReadBookingRequestView),
                              p =>
                                  {
                                      p.AddWithValue("requestId", requestId);
                                      p.AddWithValue("userId", userId);
                                  });

            return booking;
        }

        private async Task<RentItemBookingData> GetOutgoingRequest(int requestId, string userId)
        {
            const string Query = "select * from [BookingRequestsView] where [RequesterUserId] = @userId and [BookingRequestId] = @requestId;";

            var booking = await this.RunMsSqlQuery(
                              Query,
                              async command => await command.ReadItem(this.ReadBookingRequestView),
                              p =>
                                  {
                                      p.AddWithValue("userId", userId);
                                      p.AddWithValue("requestId", requestId);
                                  });

            return booking;
        }

        private async Task DeleteOutgoingRequestInternal(int requestId)
        {
            using (var context = new Context())
            {
                var request = await context.BookingRequests.Include(x => x.RentItem.Advertisement)
                                  .FirstAsync(x => x.Id == requestId);
                context.BookingRequests.Remove(request);
                await context.SaveChangesAsync();
            }

            await this.databaseLogger.LogEvent(
                LogType.Delete,
                new { Operation = "DeleteOutgoingRequest", RequestId = requestId });
        }

        private async Task ChangeRequestStatus(int requestId, BookingRequestStatus status)
        {
            BookingRequestStatus previousStatus;

            using (var context = new Context())
            {
                var request = await context.BookingRequests
                                  .Include(x => x.RentItem.Advertisement)
                                  .FirstAsync(x => x.Id == requestId);

                if (request.RentItem.Advertisement.StatusId != (int)AdvertisementStatus.Active)
                {
                    throw new EntityBadStateException("Advertisement must be in approved state.");
                }

                previousStatus = (BookingRequestStatus)request.StatusId;

                request.StatusId = (int)status;

                await context.SaveChangesAsync();
            }


            await this.databaseLogger.LogEvent(
                LogType.StatusChange,
                new { Operation = "ChangeRequestStatus", RequestId = requestId, PreviousStatus = previousStatus.ToString(), NewStatus = status.ToString() });
        }

        private async Task<List<EvtWalletInfo>> GetWalletsInfo(string userId)
        {
            var walletsInfo = await this.evtWalletsService.GetAllWallets(userId);
            return walletsInfo;
        }

        private async Task<bool> CheckItemAvailable(int rentItemId, DateTime from, DateTime to)
        {
            using (var context = new Context())
            {
                var rentItem = await context.RentItems
                                   .Include(x => x.BookingRequests)
                                   .FirstOrDefaultAsync(x => x.Id == rentItemId);

                if (rentItem == null)
                {
                    throw new ItemNotFoundException("Rent item wsa not found.");
                }

                var activeBookings = rentItem.BookingRequests
                    .Where(x => x.StatusId == (int)BookingRequestStatus.Approved)
                    .OrderBy(x => x.StartDateTime);

                return !activeBookings.Any(booking => booking.StartDateTime <= to && from <= booking.EndDateTime);
            }
        }

        private RentItemBookingData ReadBookingRequestView(IDataReader reader)
        {
            return new RentItemBookingData
            {
                RequesterUserId = (string)reader["RequesterUserId"],
                AdvertisementId = (int)reader["AdvertisementId"],
                AdvertisementOwnerUserId = (string)reader["AdvertisementOwnerUserId"],
                AdvertisementTitle = (string)reader["AdvertisementTitle"],
                BookingRequestId = (int)reader["BookingRequestId"],
                BookingRequestStatus = (int)reader["BookingRequestStatus"],
                EndDateTime = (DateTime)reader["EndDateTime"],
                FullPricePerPeriod = (decimal)reader["FullPricePerPeriod"],
                ItemCategoryId = (int)reader["ItemCategoryId"],
                RentItemName = (string)reader["RentItemName"],
                StartDateTime = (DateTime)reader["StartDateTime"],
                SecretPhrase = (string)reader["SecretPhrase"]
            };
        }
    }
}
