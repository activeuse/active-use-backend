﻿namespace Advertisements.Service.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    /// <summary>
    /// Service provides methods for advertisements management.
    /// </summary>
    public interface IAdvertisementsService
    {
        Task<int> GetAdvertisementsCount(string userId);

        /// <summary>
        /// Creates the advertisement and uploads images to BLOB storage.
        /// </summary>
        /// <param name="advertisement">The advertisement info model.</param>
        /// <param name="photos">The photos.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// ID of created advertisement.
        /// </returns>
        Task<int> Create(AdvertisementInfo advertisement, IEnumerable<FileUploadInfo> photos, string userId);

        /// <summary>
        /// Gets ad with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Advertisement entity.</returns>
        Task<AdvertisementInfo> Get(int id, string userId);

        /// <summary>
        /// Gets all ads of specified user.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="take">The take.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// List of user's ads.
        /// </returns>
        Task<List<AdvertisementInfo>> Get(int page, int take, string userId);

        /// <summary>
        /// Gets ads with status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List of user's ads with concrete status.</returns>
        Task<List<AdvertisementInfo>> GetWithStatus(AdvertisementStatus status, string userId);

        /// <summary>
        /// Deletes ad with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns><see cref="Task"/></returns>
        Task Delete(int id, string userId);

        /// <summary>
        /// Updates the specified advertisement.
        /// </summary>
        /// <param name="advertisement">The advertisement info.</param>
        /// <param name="photos">The photos.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        ///   <see cref="Task" />
        /// </returns>
        Task Update(AdvertisementInfo advertisement, IEnumerable<FileUploadInfo> photos, string userId);
    }
}