﻿namespace Advertisements.Service.DataAccess.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;

    using global::Service.Core.Entities;

    public interface ILocationsService
    {
        Task<List<CityInfo>> GetCities(int countryId);

        Task<List<CountryInfo>> GetCountries();
    }
}