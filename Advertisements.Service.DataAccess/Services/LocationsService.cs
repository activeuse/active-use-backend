﻿namespace Advertisements.Service.DataAccess.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Repositories;

    using AutoMapper;

    using global::Service.Core.Entities;

    internal sealed class LocationsService : ILocationsService
    {
        private readonly ILocationsRepository locationsRepository;
        private readonly IMapper mapper;

        public LocationsService(ILocationsRepository locationsRepository, IMapper mapper)
        {
            this.locationsRepository = locationsRepository;
            this.mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<List<CityInfo>> GetCities(int countryId)
        {
            var cities = await this.locationsRepository.GetCities(countryId);
            var mappedCities = this.mapper.Map<List<CityInfo>>(cities);

            return mappedCities;
        }

        /// <inheritdoc />
        public async Task<List<CountryInfo>> GetCountries()
        {
            var countries = await this.locationsRepository.GetCountries();
            var mappedCountries = this.mapper.Map<List<CountryInfo>>(countries);

            return mappedCountries;
        }
    }
}