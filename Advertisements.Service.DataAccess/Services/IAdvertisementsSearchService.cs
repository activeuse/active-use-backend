﻿namespace Advertisements.Service.DataAccess.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;

    using global::Service.Core.Entities;

    public interface IAdvertisementsSearchService
    {
        Task<List<AdvertisementInfo>> Search(string searchString, int? categoryId, int? cityId, string userId, int page, int pageSize);
    }
}