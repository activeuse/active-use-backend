﻿namespace Advertisements.Service.DataAccess.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Models.Views;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;
    using global::Service.Core.Entities.Views;

    public interface IBookingService
    {
        Task ApproveRequest(int requestId, string userId);

        Task RejectRequest(int requestId, string userId);

        Task DeleteOutgoingRequest(int requestId, string userId);

        Task<BookingRequestResult> Request(BookingRequestInfo request, string secretPhrase);

        Task<List<OutgoingBookingRequestInfo>> GetOutgoingRequests(string userId);

        Task<List<IncomingBookingRequestInfo>> GetIncomingRequests(string userId);
    }
}