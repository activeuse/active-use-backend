﻿namespace Advertisements.Service.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Repositories;

    using AutoMapper;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    internal sealed class AdvertisementsSearchService : IAdvertisementsSearchService
    {
        private const int DefaultPageSize = 20;

        private readonly IAdvertisementRepository advertisementRepository;

        private readonly IMapper mapper;

        public AdvertisementsSearchService(IAdvertisementRepository advertisementRepository, IMapper mapper)
        {
            this.advertisementRepository = advertisementRepository;
            this.mapper = mapper;
        }

        public async Task<List<AdvertisementInfo>> Search(string searchString, int? categoryId, int? cityId, string userId, int page, int pageSize)
        {
            var skip = page * pageSize >= 0 ? page * pageSize : 0;
            var take = pageSize > 0 ? pageSize : DefaultPageSize;

            Expression<Func<AdvertisementEntity, bool>> searchExpression = ad => ad.StatusId == (int)AdvertisementStatus.Active;

            if (!string.IsNullOrEmpty(searchString))
            {
                searchExpression = searchExpression.And(ad => (ad.RentItem.Details.Contains(searchString) || ad.Title.Contains(searchString) || ad.Description.Contains(searchString) || ad.RentItem.ItemCategory.Name.Contains(searchString)));
            }

            if (categoryId.HasValue)
            {
                searchExpression = searchExpression.And(ad => ad.RentItem.ItemCategoryId == categoryId.Value);
            }

            if (cityId.HasValue)
            {
                searchExpression = searchExpression.And(ad => ad.Location.CityId == cityId.Value);
            }

            var ads = await this.advertisementRepository.Search(searchExpression, skip, take);
            var mapped = this.mapper.Map<List<AdvertisementInfo>>(ads);
            return mapped;
        }
    }
}