﻿namespace Advertisements.Service.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using Advertisements.Service.DataAccess.Models;
    using Advertisements.Service.DataAccess.Repositories;

    using AutoMapper;

    using global::Service.Core.Cloud;
    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;
    using global::Service.Core.Logging;
    using global::Service.Core.Utils;

    /// <inheritdoc />
    internal sealed class AdvertisementsService : IAdvertisementsService
    {
        private readonly IAdvertisementRepository advertisementRepository;

        private readonly IAzureBlobStorageService azureBlobStorageService;

        private readonly IMapper mapper;

        private readonly IDatabaseLogger databaseLogger;

        /// <summary>
        /// Maximal width of thumbnail
        /// </summary>
        private readonly int thumbnailWidth;

        /// <summary>
        /// Maximal height of thumbnail
        /// </summary>
        private readonly int thumbnailHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvertisementsService"/> class.
        /// </summary>
        public AdvertisementsService(
            IAdvertisementRepository advertisementRepository, 
            IAzureBlobStorageService azureBlobStorageService, 
            IMapper mapper,
            IDatabaseLogger databaseLogger)
        {
            this.advertisementRepository = advertisementRepository;
            this.azureBlobStorageService = azureBlobStorageService;
            this.mapper = mapper;
            this.databaseLogger = databaseLogger;

            this.thumbnailWidth = int.Parse(ConfigurationManager.AppSettings["ThumbnailWidth"]);
            this.thumbnailHeight = int.Parse(ConfigurationManager.AppSettings["ThumbnailHeight"]);
        }

        /// <inheritdoc />
        public async Task<int> Create(AdvertisementInfo advertisement, IEnumerable<FileUploadInfo> photos, string userId)
        {
            var uploadedPhotos = await this.UploadInternal(photos, userId);
            var entity = this.mapper.Map<AdvertisementEntity>(advertisement);
            entity.RentItem.Photos = uploadedPhotos;
            entity.UserId = userId;
            entity.StatusId = (int)AdvertisementStatus.Active;
            var id = await this.advertisementRepository.Create(entity);
            await this.databaseLogger.LogEvent(
                LogType.Create,
                new { Entity = "Advertisement", Id = id, UserId = userId });
            return id;
        }

        /// <inheritdoc />
        public async Task<AdvertisementInfo> Get(int id, string userId)
        {
            var advertisement = await this.advertisementRepository.Get(id, userId);
            var mappedAdvertisement = this.mapper.Map<AdvertisementInfo>(advertisement);
            return mappedAdvertisement;
        }

        /// <inheritdoc />
        public async Task Update(AdvertisementInfo advertisement, IEnumerable<FileUploadInfo> photos, string userId)
        {
            throw new NotImplementedException();

            //var existingPhotos = await this.itemPhotosRepository.GetByRentItemId(advertisement.RentItem.Id);
            //await this.itemPhotosRepository.DeleteRange(existingPhotos);

            //var tasks = existingPhotos.Select(x => this.azureBlobStorageService.Delete(Path.GetFileName(x.CloudUrl)));
            //await Task.WhenAll(tasks);

            //var uploadedPhotos = await this.UploadInternal(advertisement.RentItem.Photos);
            //var entity = advertisement.ToEntity(uploadedPhotos);
            //await this.advertisementRepository.Update(entity);
        }

        /// <inheritdoc />
        public async Task Delete(int id, string userId)
        {
            var ad = await this.advertisementRepository.Get(id, userId);
            if (ad == null)
            {
                return;
            }

            await this.advertisementRepository.Delete(id, userId);

            await this.databaseLogger.LogEvent(
                LogType.Delete,
                new { Entity = "Advertisement", Id = id, UserId = userId });
        }

        /// <inheritdoc />
        public async Task<List<AdvertisementInfo>> Get(int page, int take, string userId)
        {
            var advertisements = await this.advertisementRepository.Get(page, take, userId);
            var mappedAdvertisements = this.mapper.Map<List<AdvertisementInfo>>(advertisements);
            return mappedAdvertisements;
        }

        public async Task<int> GetAdvertisementsCount(string userId)
        {
            return await this.advertisementRepository.GetAdvertisementsCount(userId);
        }

        /// <inheritdoc />
        public async Task<List<AdvertisementInfo>> GetWithStatus(AdvertisementStatus status, string userId)
        {
            var advertisements = await this.advertisementRepository.GetWithStatus(status, userId);
            var mappedAdvertisements = this.mapper.Map<List<AdvertisementInfo>>(advertisements);
            return mappedAdvertisements;
        }

        /// <summary>
        /// Handles images upload.
        /// </summary>
        private async Task<List<ItemPhotoEntity>> UploadInternal(IEnumerable<FileUploadInfo> imagesToUpload, string userId)
        {
            var uploadTasks = imagesToUpload.Select(x => this.UploadSinglePhoto(x, userId));
            var uploadedPhotos = (await Task.WhenAll(uploadTasks)).SelectMany(x => x).ToList();
            return uploadedPhotos;
        }

        /// <summary>
        /// Uploads single photo and thumb to BLOB container.
        /// </summary>
        private async Task<List<ItemPhotoEntity>> UploadSinglePhoto(FileUploadInfo fileInfo, string userId)
        {
            var uploadedPhotos = new List<ItemPhotoEntity>();

            var format = ImageFormat.Jpeg;
            switch (Path.GetExtension(fileInfo.Name))
            {
                case ".jpg":
                case ".jpeg":
                    format = ImageFormat.Jpeg;
                    break;
                case ".bmp":
                    format = ImageFormat.Bmp;
                    break;
                case ".png":
                    format = ImageFormat.Png;
                    break;
            }

            // upload original image
            var image = new Bitmap(new MemoryStream(fileInfo.Data));
            var imageName = Path.Combine(userId, $"{Guid.NewGuid()}_{fileInfo.Name}");
            var imageUrl = await this.azureBlobStorageService.Upload(new MemoryStream(fileInfo.Data), imageName);
            var thumbName = Path.Combine(userId, "thumb_" + $"{Guid.NewGuid()}_{fileInfo.Name}");

            var imageSize = image.Size;
            string thumbUrl;
            Size thumbSize;

            using (image)
            {
                // upload thumb
                thumbSize = ImageUtils.ResizeKeepAspect(image, this.thumbnailWidth, this.thumbnailHeight);
                var thumb = image.GetThumbnailImage(thumbSize.Width, thumbSize.Height, null, IntPtr.Zero);
                using (thumb)
                {
                    using (var thumbStream = new MemoryStream())
                    {
                        thumb.Save(thumbStream, format);
                        thumbStream.Seek(0, SeekOrigin.Begin);
                        thumbUrl = await this.azureBlobStorageService.Upload(thumbStream, thumbName);
                    }
                }
            }

            uploadedPhotos.Add(new ItemPhotoEntity
            {
                Width = imageSize.Width,
                Height = imageSize.Height,
                CloudUrl = imageUrl,
                Name = fileInfo.Name,
                IsThumbnail = false
            });

            uploadedPhotos.Add(new ItemPhotoEntity
            {
                Width = thumbSize.Width,
                Height = thumbSize.Height,
                CloudUrl = thumbUrl,
                Name = thumbName,
                IsThumbnail = true
            });

            await this.databaseLogger.LogEvent(
                LogType.Upload,
                new { Entity = "Item Photo", ThumbUrl = thumbUrl, ImageUrl = imageUrl });

            return uploadedPhotos;
        }
    }
}