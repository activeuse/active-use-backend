﻿namespace Advertisements.Service.DataAccess.Context
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using global::Service.Core.Entities;
    using global::Service.Core.Logging;

    internal sealed class Context : DbContext
    {
        public Context()
            : base("Advertisements")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<Context>());
            // ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 600;

            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            objectContext.SavingChanges += (sender, args) =>
                {
                    var now = DateTime.UtcNow;
                    foreach (var entry in this.ChangeTracker.Entries<IDatedEntity>())
                    {
                        var entity = entry.Entity;
                        switch (entry.State)
                        {
                            case EntityState.Added:
                                entity.CreatedAt = now;
                                entity.UpdatedAt = now;
                                break;
                            case EntityState.Modified:
                                entity.UpdatedAt = now;
                                break;
                        }
                    }

                    this.ChangeTracker.DetectChanges();
                };
        }

        public DbSet<AdvertisementEntity> Advertisements { get; set; }

        public DbSet<RentItemEntity> RentItems { get; set; }

        public DbSet<ItemCategoryEntity> Categories { get; set; }

        public DbSet<CurrencyEntity> Currencies { get; set; }

        public DbSet<RentPeriodEntity> RentPeriods { get; set; }

        public DbSet<AdvertisementStatusEntity> AdvertisementStatus { get; set; }

        public DbSet<ItemPhotoEntity> ItemPhotos { get; set; }

        public DbSet<CountryEntity> Countries { get; set; }

        public DbSet<CityEntity> Cities { get; set; }

        public DbSet<DeliveryTypeEntity> DeliveryTypes { get; set; }

        public DbSet<GenderEntity> Genders { get; set; }

        public DbSet<MeasureUnitEntity> MeasureUnits { get; set; }

        public DbSet<BookingRequestStatusEntity> BookingRequestStatus { get; set; }

        public DbSet<BookingRequestEntity> BookingRequests { get; set; }

        //public DbSet<BookingEntity> Bookings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}