﻿namespace Advertisements.Service.DataAccess.Mappings
{
    using System.Collections.Generic;
    using System.Linq;

    using Advertisements.Service.DataAccess.Models;

    using AutoMapper;

    using global::Service.Core.Entities;
    using global::Service.Core.Entities.Enums;

    public sealed class DataAccessMappings : Profile
    {
        public DataAccessMappings()
        {
            this.MapInfoModelsToEntities();
            this.MapEntitiesToInfoModels();
        }

        private void MapEntitiesToInfoModels()
        {
            this.CreateMap<CityEntity, CityInfo>();
            this.CreateMap<CountryEntity, CountryInfo>();

            this.CreateMap<ItemDeliveryEntity, ItemDeliveryInfoModel>()
                .ForMember(dest => dest.DeliveryType, opt => opt.MapFrom(src => src.DeliveryTypeId));

            this.CreateMap<LocationEntity, LocationInfo>()
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.City.Country.Name))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City.Name))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.City.CountryId))
                .ForMember(dest => dest.NativeCountryName, opt => opt.MapFrom(src => src.City.Country.Name));

            this.CreateMap<ItemPackageEntity, ItemPackageInfo>();
            this.CreateMap<RentConfigurationEntity, RentConfigurationInfo>()
                .ForMember(dest => dest.RentPeriod, opt => opt.MapFrom(src => src.RentPeriodId))
                .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.CurrencyId));

            this.CreateMap<RentItemPropertyEntity, RentItemPropertyInfo>();

            this.CreateMap<ItemDimensionsEntity, RentItemDimensionsInfo>()
                .ForMember(dest => dest.MeasureUnit, opt => opt.MapFrom(src => src.MeasureUnitId));

            this.CreateMap<ItemPhotoEntity, ItemPhotoInfo>();

            this.CreateMap<AdvertisementEntity, AdvertisementInfo>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.StatusId))
                .ForMember(dest => dest.CoverPhoto, opt => opt.MapFrom(src => src.RentItem.Photos.First(x => x.IsThumbnail)))
                .ForMember(dest => dest.RentConfiguration, opt => opt.MapFrom(src => src.RentConfiguration.First()));

            this.CreateMap<RentItemEntity, RentItemInfo>()
                .ForMember(dest => dest.ItemCategory, opt => opt.MapFrom(src => src.ItemCategoryId))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.GenderId))
                .ForMember(dest => dest.DeliveryTypes, opt => opt.MapFrom(src => src.Deliveries));
        }

        private void MapInfoModelsToEntities()
        {
            this.CreateMap<ItemDeliveryInfoModel, ItemDeliveryEntity>()
                .ForMember(dest => dest.RentItem, opt => opt.Ignore())
                .ForMember(dest => dest.DeliveryType, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RentItemId, opt => opt.Ignore())
                .ForMember(dest => dest.DeliveryTypeId, opt => opt.MapFrom(src => src.DeliveryType));

            this.CreateMap<LocationInfo, LocationEntity>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.City, opt => opt.Ignore())
                .ForMember(dest => dest.Advertisement, opt => opt.Ignore());

            this.CreateMap<ItemPackageInfo, ItemPackageEntity>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RentItem, opt => opt.Ignore())
                .ForMember(dest => dest.RentItemId, opt => opt.Ignore());

            this.CreateMap<RentConfigurationInfo, RentConfigurationEntity>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Advertisement, opt => opt.Ignore())
                .ForMember(dest => dest.AdvertisementId, opt => opt.Ignore())
                .ForMember(dest => dest.DiscountSettings, opt => opt.Ignore())
                .ForMember(dest => dest.RentPeriod, opt => opt.Ignore())
                .ForMember(dest => dest.Currency, opt => opt.Ignore())
                .ForMember(dest => dest.RentPeriodId, opt => opt.MapFrom(src => src.RentPeriod))
                .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.Currency));

            this.CreateMap<RentItemPropertyInfo, RentItemPropertyEntity>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RentItem, opt => opt.Ignore())
                .ForMember(dest => dest.RentItemId, opt => opt.Ignore());

            this.CreateMap<RentItemDimensionsInfo, ItemDimensionsEntity>()
                .ForMember(dest => dest.MeasureUnitId, opt => opt.MapFrom(src => src.MeasureUnit))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RentItem, opt => opt.Ignore())
                .ForMember(dest => dest.MeasureUnit, opt => opt.Ignore());

            this.CreateMap<ItemPhotoInfo, ItemPhotoEntity>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RentItem, opt => opt.Ignore())
                .ForMember(dest => dest.RentItemId, opt => opt.Ignore());

            this.CreateMap<AdvertisementInfo, AdvertisementEntity>()
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.StatusId, opt => opt.UseValue((int)AdvertisementStatus.New))
                .ForMember(dest => dest.RentConfiguration, opt => opt.ResolveUsing(src => new List<RentConfigurationInfo> { src.RentConfiguration }));

            this.CreateMap<RentItemInfo, RentItemEntity>()
                .ForMember(dest => dest.ItemCategory, opt => opt.Ignore())
                .ForMember(dest => dest.Gender, opt => opt.Ignore())
                .ForMember(dest => dest.Advertisement, opt => opt.Ignore())
                .ForMember(dest => dest.BookingRequests, opt => opt.Ignore())
                .ForMember(dest => dest.Photos, opt => opt.Ignore())
                .ForMember(dest => dest.GenderId, opt => opt.MapFrom(src => (int?)src.Gender))
                .ForMember(dest => dest.ItemCategoryId, opt => opt.MapFrom(src => src.ItemCategory))
                .ForMember(dest => dest.Deliveries, opt => opt.MapFrom(src => src.DeliveryTypes));
        }
    }
}